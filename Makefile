export GCCN64PREFIX = mips64-
export CHKSUM64PATH = tools/chksum64
export MKDFSPATH = tools/mkdfs
export HEADERPATH = /usr/local/gcc-mips64vr4300/mips64/lib
export N64TOOL = tools/n64tool

export CC = $(GCCN64PREFIX)gcc
export AS = $(GCCN64PREFIX)as
export LD = $(GCCN64PREFIX)ld
export AR = $(GCCN64PREFIX)ar
export OBJCOPY = $(GCCN64PREFIX)objcopy

export OBJDIR = ./obj
export SRCDIR = ./src

export LIBKATEGLPATH = $(CURDIR)/kategl
export LIBDRAGONPATH = $(CURDIR)/libdragon
export LIBCPPPATH    = $(CURDIR)/n64cpp

GAMEPATH = $(CURDIR)/game
LIBKATEGLPATH = $(CURDIR)/kategl

HEADERNAME = header
ifeq ($(N64_BYTE_SWAP),true)
ROM_EXTENSION = .v64
N64_FLAGS = -b -l 4M -h $(HEADERPATH)/$(HEADERNAME) -o $(PROG_NAME)$(ROM_EXTENSION) $(GAMEPATH)/$(OBJDIR)/$(PROG_NAME).bin
else
ROM_EXTENSION = .z64
N64_FLAGS = -l 4M -h $(HEADERPATH)/$(HEADERNAME) -o $(PROG_NAME)$(ROM_EXTENSION) $(GAMEPATH)/$(OBJDIR)/$(PROG_NAME).bin
endif
 
PROG_NAME = hello

.PHONY: all clean

###
all: kategl libdragon $(PROG_NAME)$(ROM_EXTENSION)

$(PROG_NAME)$(ROM_EXTENSION): textures.dfs libn64cpp.a libdragon.a libkategl.a hello.elf
	$(OBJCOPY) $(GAMEPATH)/$(OBJDIR)/$(PROG_NAME).elf $(GAMEPATH)/$(OBJDIR)/$(PROG_NAME).bin -O binary
	rm -f $(PROG_NAME)$(ROM_EXTENSION)
	cd textures; python convert.py; cd ..
	#tools/mkdfs audio.dfs ./game/src/assets/audio
	tools/mkdfs test.dfs ./tools/pakcomp/out
	#$(N64TOOL) $(N64_FLAGS) -t "Space Shooter" -s 1M audio.dfs
	$(N64TOOL) $(N64_FLAGS) -t "Space Shooter" -s 1M test.dfs
	$(CHKSUM64PATH) $(PROG_NAME)$(ROM_EXTENSION)

test.dfs:
	tools/mkdfs test.dfs tools/pakcomp/test.pak

libkategl.a:
	$(MAKE) -C ./kategl

libdragon.a:
	$(MAKE) -C ./libdragon

libn64cpp.a:
	$(MAKE) -C ./n64cpp

hello.elf:
	$(MAKE) -C ./game

clean:
	$(MAKE) -C ./kategl clean
	$(MAKE) -C ./libdragon clean
	$(MAKE) -C ./game clean
	$(MAKE) -C ./n64cpp clean
