#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

typedef struct {
    std::string name;         // File name.
    uint8_t *data;                // Pointer to the data this lump contains. 
    uint32_t length;              // Length of the lump's data.
    uint32_t starting_offset;     // Where in the WAD file does the lump begin?
} PAK_File;

std::streampos streamstart;

void PAK_WriteHeader(std::ofstream *stream, uint32_t file_count, uint32_t dir_offset, uint32_t dir_size)
{
    printf("Writing header to stream: %d files, directory offset is %x, directory is %d bytes\n", file_count, dir_offset, dir_size);

    auto curpos = stream->tellp();          // get our current stream position
    stream->seekp(0, std::ios::beg);        // relocate to the beginning of the PAK
    stream->write("PACK", 4);
    stream->write(reinterpret_cast<const char *>(&dir_offset), 4); // location of directory
    stream->write(reinterpret_cast<const char *>(&dir_size), 4); // size of directory
    stream->seekp(curpos, std::ios::beg);   // back to the old position
}

// Writes a lump to the output stream. Updates the PAK_File with the offset that we wrote to.
void PAK_WriteFile(std::ofstream *stream, PAK_File *file)
{
    file->starting_offset = (stream->tellp() - streamstart);
    stream->write(reinterpret_cast<const char *>(file->data), file->length);
    printf("Wrote file %s to stream at %08X, length is %d bytes\n", file->name.c_str(), file->starting_offset, file->length);
}

// Write the directory to the WAD file. Returns the directory location.
std::streampos PAK_WriteDirectory(std::ofstream *stream, std::vector<PAK_File *> files)
{  
    printf("Writing directory to stream\n");

    auto dir_location = stream->tellp();
    for(auto &file : files)
    {
        stream->write(file->name.c_str(), 56);
        stream->write(reinterpret_cast<const char *>(&(file->starting_offset)), 4);     // filepos
        stream->write(reinterpret_cast<const char *>(&(file->length)), 4);              // size

        printf("Wrote directory entry: file %s at %08X, size %d bytes\n", file->name.c_str(), file->starting_offset, file->length);
    }

    return dir_location;
}

int main()
{
    std::vector<PAK_File *> vecFiles = std::vector<PAK_File *>();
    std::ofstream pak_out("output.pak", std::ios_base::binary);

    streamstart = pak_out.tellp();

    pak_out.seekp(64, std::ios::beg); // skip 12 bytes into the stream

    printf("PAK compiler for N64\n");

    // PAK_File *testfile = new PAK_File;
    // std::string teststring = "This is a test string.";
    // testfile->data = (uint8_t *)teststring.c_str();
    // testfile->length = teststring.size();
    // testfile->name = "TEST.TXT";
    // vecFiles.push_back(testfile);
    // PAK_WriteFile(&pak_out, testfile);

    std::string path = "./data";
    for (const auto & entry : std::filesystem::directory_iterator(path))
    {
        std::cout << entry.path() << std::endl;
    }

    auto dir_offset = PAK_WriteDirectory(&pak_out, vecFiles);
    PAK_WriteHeader(&pak_out, vecFiles.size(), (uint32_t)dir_offset, 64*vecFiles.size());

    return 0;
}