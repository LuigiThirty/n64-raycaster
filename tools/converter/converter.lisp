#!/usr/local/bin/sbcl --script

(require "asdf")
(asdf:load-system :uiop)

(load "triangles.lisp")

(defvar *CUBE-FILE* "cone.obj")
(defvar *WAVEFRONT-DELIMITER* " ")
(defvar *WAVEFRONT-STATEMENT-TYPES* '(("v"  . 'VERTEX-POSITION)
				     ("vn" . 'VERTEX-NORMAL)
				     ("vt" . 'VERTEX-TEXTURE)
				     ("f"  . 'FACE)
				     ("mtllib" . 'MATERIAL-REFERENCE)
				     ("usemtl" . 'USE-MATERIAL)
				     ("#"  . 'COMMENT)
				     ("o"  . 'OBJECT-NAME)))

;;; global lists
(defvar *VERTEX-POSITION-LIST* '())
(defvar *VERTEX-NORMAL-LIST* '())

(defvar *FACE-VERTEX-POSITION-INDEX-LIST* '())
(defvar *FACE-VERTEX-TEXTURE-INDEX-LIST* '())
(defvar *FACE-VERTEX-NORMAL-INDEX-LIST* '())

(defun wavefront-statement-type (keyword)  
  (caddr (assoc keyword *WAVEFRONT-STATEMENT-TYPES* :test #'equal)))
;;

(defun round-to (number precision &optional (what #'round))
    (let ((div (expt 10 precision)))
         (/ (funcall what (* number div)) div)))

;;;;;;;;;;;;;;;;;;
; Wavefront command processing
;;;;;;;;;;;;;;;;;;

(defmacro append-to-list-in-place (listname parameters)
  `(setq ,listname (append ,listname (list ,parameters))))

(defun process-wavefront-vertex (output-list parameters)
  (format t "~a " parameters)
  (let ((x (with-input-from-string (val (nth 0 parameters)) (read val)))
        (y (with-input-from-string (val (nth 1 parameters)) (read val)))
        (z (with-input-from-string (val (nth 2 parameters)) (read val))))
    (append-to-list-in-place output-list (make-instance 'triangle-vertex :vX x :vY y :vZ z))))  

(defun process-wavefront-vertex-position (parameters)
  (format t "~a " parameters)
  (let ((x (with-input-from-string (val (nth 0 parameters)) (read val)))
        (y (with-input-from-string (val (nth 1 parameters)) (read val)))
        (z (with-input-from-string (val (nth 2 parameters)) (read val))))
    (append-to-list-in-place *VERTEX-POSITION-LIST* (make-instance 'triangle-vertex :vX x :vY y :vZ z))))

(defun process-wavefront-vertex-normal (parameters)
  (format t "~a " parameters)
  (let ((x (with-input-from-string (val (nth 0 parameters)) (read val)))
        (y (with-input-from-string (val (nth 1 parameters)) (read val)))
        (z (with-input-from-string (val (nth 2 parameters)) (read val))))
    (append-to-list-in-place *VERTEX-NORMAL-LIST* (make-instance 'triangle-vertex :vX x :vY y :vZ z))))

;;
(defun collect-face-from-lists-at-position (position &rest lists)
  "Collect the Nth value of the three provided lists into a result list. Converts 1-indexed .OBJ lists to 0-index lists. Used for faces."
  (map 'list
       #'(lambda (x) (handler-case (- (parse-integer (nth position x) :junk-allowed t) 1)
		       (type-error (e)
				   nil)))
       lists))

(defun build-wavefront-face-lists (parameters)
  (let ((face-vertex-one (uiop:split-string (car parameters) :separator "/"))
	(face-vertex-two (uiop:split-string (cadr parameters) :separator "/"))
	(face-vertex-three (uiop:split-string (caddr parameters) :separator "/")))

    (append-to-list-in-place *FACE-VERTEX-POSITION-INDEX-LIST* (collect-face-from-lists-at-position 0 face-vertex-one face-vertex-two face-vertex-three))
    (append-to-list-in-place *FACE-VERTEX-TEXTURE-INDEX-LIST* (collect-face-from-lists-at-position 1 face-vertex-one face-vertex-two face-vertex-three))
    (append-to-list-in-place *FACE-VERTEX-NORMAL-INDEX-LIST* (collect-face-from-lists-at-position 2 face-vertex-one face-vertex-two face-vertex-three))))

(defun process-wavefront-face (parameters)
  (format t "~a " parameters)
  (build-wavefront-face-lists parameters))

;;;;;;;;;;;;;;;;;;
(defun parse-line (line)
  (let ((keywords (uiop:split-string line :separator *WAVEFRONT-DELIMITER*)))
    (let ((statement-type (wavefront-statement-type (car keywords))))
 
      ; The first word in an OBJ line is the statement. Determine the statement type.
      (format t "~a " statement-type)

      (case statement-type
	    ('VERTEX-POSITION (process-wavefront-vertex-position (cdr keywords)))
	    ('VERTEX-NORMAL (process-wavefront-vertex-normal (cdr keywords)))
	    ('FACE (process-wavefront-face (cdr keywords))))

      (format t "~%"))))

(with-open-file (model *CUBE-FILE* :direction :input :if-does-not-exist nil)
  (loop for line = (read-line model nil)
	while line do (parse-line line)))

(format t "~a ~a~%" "vertex position list" *VERTEX-POSITION-LIST*)

(format t "~a ~a~%" "face position list" *FACE-VERTEX-POSITION-INDEX-LIST*)
(format t "~a ~a~%" "face texture list" *FACE-VERTEX-TEXTURE-INDEX-LIST*)
(format t "~a ~a~%" "face normal list" *FACE-VERTEX-NORMAL-INDEX-LIST*)

;; Now turn the faces into coordinates and junk.
(defun face-with-vertex-data (face)
  (list (nth (nth 0 face) *VERTEX-POSITION-LIST*) (nth (nth 1 face) *VERTEX-POSITION-LIST*) (nth (nth 2 face) *VERTEX-POSITION-LIST*)))

;; Convert to a C header for the N64 microcode.
(defun triangle-c-header (vX vY vZ vR vG vB vA vS vT)
  (format nil "{ ~d, ~d, ~d, 0, ~d, ~d, ~d, ~d, ~d, ~d }," vX vY vZ vR vG vB vA vS vT))

(defun print-model-vertex-struct ()
  (format t "static BASIC_Vertex ~a[] = { ~%" "test")
  
  (loop for face in *FACE-VERTEX-POSITION-INDEX-LIST*
	do (progn
	     (let ((color (random 255))
		   (vertex-one (nth (nth 0 face) *VERTEX-POSITION-LIST*))
		   (vertex-two (nth (nth 1 face) *VERTEX-POSITION-LIST*))
		   (vertex-three (nth (nth 2 face) *VERTEX-POSITION-LIST*)))
	       (map
		'list
		(lambda (vertex-list)
		  (format t "~a~%" (triangle-c-header
				    (* 10 (round-to (slot-value vertex-list 'vX) 1)) (* 10 (round-to (slot-value vertex-list 'vY) 1)) (* 10 (round-to (slot-value vertex-list 'vZ) 1))
				    color color color 255
				    0 0)))
		(list vertex-one vertex-two vertex-three))
	       (format t "~%"))))
  
  (format t "};~%"))

(print-model-vertex-struct)
