// Generated by Nintendo 64 Texture Converter
// By buu342
// Size = 16x19
// Type = 8-Bit Color Index (G_IM_FMT_CI)

unsigned char uship6[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x03, 0x04, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x05, 0x02, 0x03, 0x05, 0x04, 0x04, 0x04, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x06, 0x01, 0x03, 0x06, 0x06, 0x06, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x04, 0x02, 0x01, 0x03, 0x06, 0x06, 0x06, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03, 0x06, 0x06, 0x06, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x07, 0x01, 0x08, 0x09, 0x09, 0x09, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x05, 0x01, 0x0A, 0x08, 0x0B, 0x0B, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x03, 0x01, 0x0A, 0x0B, 0x0B, 0x0B, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x06, 0x01, 0x0A, 0x0B, 0x0B, 0x0B, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x0C, 0x01, 0x0A, 0x0B, 0x0B, 0x0B, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x0C, 0x01, 0x0B, 0x09, 0x09, 0x0D, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x08, 0x01, 0x01, 0x0E, 0x0F, 0x08, 0x0B, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x08, 0x0D, 0x01, 0x0A, 0x0B, 0x0B, 0x0B, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x08, 0x0D, 0x01, 0x0F, 0x0B, 0x0B, 0x0B, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x08, 0x0D, 0x01, 0x08, 0x0B, 0x0B, 0x0B, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x08, 0x0D, 0x01, 0x08, 0x0B, 0x0B, 0x0B, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x01, 0x08, 0x0B, 0x01, 0x08, 0x0B, 0x0B, 0x0B, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 
};

unsigned short uship6_tlut[] = {
    0xBEEE, 0x0843, 0x51C7, 0x7B0F, 0x728B, 0x8B91, 0x6249, 0x9C15, 0x5AD7, 0x318D, 0x7BDF, 0x5295, 0x3105, 0x4211, 0x8C63, 0x6B5B, 
};