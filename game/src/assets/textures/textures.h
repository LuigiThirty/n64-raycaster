#ifndef TEXTURES_H
#define TEXTURES_H

#include "assets/textures/_identifiers.h"

#include "assets/textures/fonts/kernal.h"
#include "assets/textures/fonts/kernal_small.h"
#include "assets/textures/fonts/kernal_4bpp.h"
#include "assets/textures/rights.h"
#include "assets/textures/tyrianbullet.h"

// Fonts
#include "assets/textures/fonts/kernal_upper.h"
#include "assets/textures/fonts/kernal_lower.h"
#include "assets/textures/fonts/kernal_numbers.h"
#include "assets/textures/fonts/arialcaps.h"
#include "assets/textures/fonts/ariallower.h"
#include "assets/textures/fonts/ariallower2.h"

// USP Talon - 5 frames
#include "assets/textures/ships/talon/usptalon-left2.h"
#include "assets/textures/ships/talon/usptalon-left1.h"
#include "assets/textures/ships/talon/usptalon-middle.h"
#include "assets/textures/ships/talon/usptalon-right1.h"
#include "assets/textures/ships/talon/usptalon-right2.h"

// U-Ship - 4 frames
#include "assets/textures/ships/uship/uship1.h"
#include "assets/textures/ships/uship/uship2.h"
#include "assets/textures/ships/uship/uship3.h"
#include "assets/textures/ships/uship/uship4.h"
#include "assets/textures/ships/uship/uship5.h"
#include "assets/textures/ships/uship/uship6.h"
#include "assets/textures/ships/uship/uship7.h"
#include "assets/textures/ships/uship/uship8.h"

// #include "assets/textures/xmas1996.h"

// Toy stuff
#include "assets/textures/prohibitory.h"

#endif