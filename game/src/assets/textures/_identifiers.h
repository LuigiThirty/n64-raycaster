#ifndef TEXTURES_IDENTIFIERS_H
#define TEXTURES_IDENTIFIERS_H

#define TEX_NULL        "NULL"
#define TEX_USHIP       "USHIP"
#define TEX_BULLETS     "BULLETS"
#define TEX_F_KERNAL    "F_KERNAL"

#define TEX_F_ARIALCAPS  "F_ARIAL_C"
#define TEX_F_ARIALLOWER "F_ARIAL_L"

#define TEX_F_ARIAL1    "F_ARIALL1"
#define TEX_F_ARIAL2    "F_ARIALL2"
#define TEX_F_ARIAL3    "F_ARIALL3"
#define TEX_F_ARIAL4    "F_ARIALL4"

#define TEX_TALON       "TALON"
#define TEX_TALONLL     "TALONLL"
#define TEX_TALONL      "TALONL"
#define TEX_TALONRR     "TALONRR"
#define TEX_TALONR      "TALONR"

#define TEX_USHIP1      "USHIP1"
#define TEX_USHIP2      "USHIP2"
#define TEX_USHIP3      "USHIP3"
#define TEX_USHIP4      "USHIP4"
#define TEX_USHIP5      "USHIP5"
#define TEX_USHIP6      "USHIP6"
#define TEX_USHIP7      "USHIP7"
#define TEX_USHIP8      "USHIP8"

#endif