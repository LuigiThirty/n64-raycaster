#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <libdragon.h>
#include <string.h>

#include "fixed.h"
#include "mmio.h"
#include "regs.h"
#include "64drive.h"
#include "rsp_overlay.h"
#include "pak.hpp"

#include "microcode/basic.h"
#include "assets/models/models.hpp"
#include "display_lists/display_lists.h"
#include "kategl/kategl.hpp"

#define DEBUG_ENABLED

#define UNCACHED_MTX(X) ( (KateGL::FX_Matrix44 *)UncachedAddr(X) )

static KateGL::FX_Matrix44 perspective_matrix __attribute__((aligned(8)));
static KateGL::FX_Matrix44 view_matrix __attribute__((aligned(8)));
static KateGL::FX_Matrix44 model_matrix __attribute__((aligned(8)));
static KateGL::FX_Matrix44 transformation_matrix __attribute__((aligned(8)));

static KateGL::FX_Matrix44 model_translation __attribute__((aligned(8)));
static KateGL::FX_Matrix44 model_rotation __attribute__((aligned(8)));
static KateGL::FX_Matrix44 model_scale __attribute__((aligned(8)));

display_list_t my_cool_display_list[1024] __attribute__((aligned(8)));
display_list_t *DMEM_display_list = (display_list_t *)0xA4000000;

/* Raycaster microcode symbols. */
extern const char basic_ucode_start;
extern const char basic_ucode_end;

extern const char vertex_ucode_start;
extern const char vertex_ucode_end;
