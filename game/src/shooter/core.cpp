#include "shooter/core.h"
#include "assets/textures/_identifiers.h"
#include "shooter/mobj/kinds/_kinds.h"
#include "display_lists/display_lists.h"
#include "shooter/enemies.hpp"

#include <string.h>
#include <stdio.h>
#include <array.h>

#define MAX_ENEMY_WAVES 50

bool enable_controls = true;

LevelStatus levelStatus;
std::array<LevelObjectWave, MAX_ENEMY_WAVES> waves;

void Shooter::ProcessPlayerControls()
{
    static bool debounceA = false;

    // Read the controller state.
    controller_scan(); // Scan the attached controllers.
    auto cdata = get_keys_held();

    if(enable_controls)
    {
        if(cdata.c[0].left) 	{ playerLeftForFrames++; PLAYER_MOBJECT->position.x -= 0x00010000; } else playerLeftForFrames = 0;
        if(cdata.c[0].right)	{ playerRightForFrames++; PLAYER_MOBJECT->position.x += 0x00010000; } else playerRightForFrames = 0;
        if(cdata.c[0].up) 		PLAYER_MOBJECT->position.y -= 0x00010000;
        if(cdata.c[0].down) 	PLAYER_MOBJECT->position.y += 0x00010000;
    }

    if(cdata.c[0].A && !debounceA)
    {
        // Create a new bullet and fire it.
        auto obj = mobjectManager.Create(MOBJ_K_BULLET);
        ((Bullet *)obj)->Fire(PLAYER_MOBJECT);

        debounceA = true;
    }
    else if(!cdata.c[0].A)
    {
        debounceA = false;
    }

}

void Shooter::BulletBehavior()
{
    // Destroy any out-of-bounds bullet mobjs.
    for(int i=0; i<MAX_MOBJECTS; i++)
    {
        MObjectsEntry obj = mobjectManager.mobjects[i];

        if(obj.kind == MOBJ_K_BULLET && obj.mobject->state == Bullet::STATE_MOVING)
        {
            if(obj.mobject->position.y < 0)
            {
                mobjectManager.Dispose(obj.mobject);
            }
        }
    }
}

int half_bullet_width;

void ProcessBulletCollision(KateGL::MObject *bullet, KateGL::MObject *target)
{
    mobjectManager.Dispose(bullet);
    mobjectManager.Dispose(target);
}

void Shooter::CheckBulletCollision()
{
    // Get the U-Ship texture info so we know the bounding box we need to examine.
    KateGL::TextureListEntry *uship_tle = KateGL::textureManager.GetTextureByName(TEX_USHIP1);
    KateGL::TextureListEntry *bullet_tle = KateGL::textureManager.GetTextureByName(TEX_BULLETS);

    half_bullet_width = FX_FromInt(bullet_tle->info->size.x / 2);

    // Get a list of U-Ships.
    // TODO: Rig up the mobject manager to return an array of pointers to all mobjs of a specific kind.
    KateGL::MObject *uships[256];
    int uships_count = 0;
    for(int i=0; i<MAX_MOBJECTS; i++)
    {            
        MObjectsEntry obj = mobjectManager.mobjects[i];
        if(obj.kind == MOBJ_K_USHIP)
        {
            uships[uships_count++] = obj.mobject;
        }
    }

    for(int i=0; i<MAX_MOBJECTS; i++)
    {
        // Is a bullet inside the bounding box of a U-Ship?
        MObjectsEntry obj = mobjectManager.mobjects[i];

        if(obj.kind == MOBJ_K_BULLET && obj.mobject->state == Bullet::STATE_MOVING)
        {
            KateGL::Point3D bullet_top_center = obj.mobject->position; bullet_top_center.x += half_bullet_width;

            for(int uship=0; uship<uships_count; uship++)
            {
                if(KateGL::PointIsInsideTextureBound2D(uship_tle, uships[uship]->position, bullet_top_center))
                {
                    Point2D local;
                    local.x = FX_ToInt(bullet_top_center.x - uships[uship]->position.x);
                    local.y = FX_ToInt(bullet_top_center.y - uships[uship]->position.y);

                    bool bg_pixel = KateGL::TexelContainsBackgroundPixel(uship_tle, local);

                    if(bg_pixel == false)
                    {
                        ProcessBulletCollision(obj.mobject, uships[uship]);
                    }
                }
            }
        }
    }
}

void Shooter::Setup()
{
    levelStatus.scroll_units_per_frame = FX_FromInt(1);
    levelStatus.scroll_units_travelled = 0;
    playerLeftForFrames = 0;
    playerRightForFrames = 0;

    {
        // Create the player object.
        PLAYER_MOBJECT = mobjectManager.Create(MOBJ_K_PLAYER);
        PLAYER_MOBJECT->position.x = FX_FromInt(40);
        PLAYER_MOBJECT->position.y = FX_FromInt(160);
        PLAYER_MOBJECT->team = KateGL::TEAM_GOOD;
    }

    // for(int j=0; j<1; j++)
    // {
    //     for(int i=0; i<15; i++)
    //     {
    //         // create some U-Ships.
    //         UShip *mobj = static_cast<UShip *>(mobjectManager.Create(MOBJ_K_USHIP));
    //         mobj->position.x = 10+(i*20);
    //         mobj->position.y = 20+(j*20);
    //         mobj->team = KateGL::TEAM_BAD;
    //         mobj->current_frame = j % mobj->number_of_frames;
    //     }
    // }

    for(int j=0; j<1; j++)
    {
        for(int i=0; i<30; i++)
        {
            // create some bullets.
            auto *mobj = static_cast<Bullet *>(mobjectManager.Create(MOBJ_K_BULLET));
            mobj->position.x = FX_FromInt(10 + (i*10));
            mobj->position.y = FX_FromInt(10 + (j*20));
            mobj->team = KateGL::TEAM_BAD;
            mobj->state = Bullet::STATE_MOVING;
            mobj->velocity.y = 0x00008000;
        }
    }

    for(int i=0;i<MAX_ENEMY_WAVES;i++)
    {
        waves[i].y_position == -1;
    }

    waves[0].x_position = FX_FromInt(200);
    waves[0].y_position = FX_FromInt(300);
    waves[0].numObjects = 5;
    waves[0].movement = LevelObjectMovement::StraightDown;
    waves[0].speedFactor = FX_FromFloat(0.5);

    waves[0].objects[0].kind = MOBJ_K_USHIP;
    waves[0].objects[0].x_offset = FX_FromInt(0);
    waves[0].objects[0].y_offset = FX_FromInt(0);

    waves[0].objects[1].kind = MOBJ_K_USHIP;
    waves[0].objects[1].x_offset = FX_FromInt(-15);
    waves[0].objects[1].y_offset = FX_FromInt(-10);

    waves[0].objects[2].kind = MOBJ_K_USHIP;
    waves[0].objects[2].x_offset = FX_FromInt(15);
    waves[0].objects[2].y_offset = FX_FromInt(-10);

    waves[0].objects[3].kind = MOBJ_K_USHIP;
    waves[0].objects[3].x_offset = FX_FromInt(-30);
    waves[0].objects[3].y_offset = FX_FromInt(-20);

    waves[0].objects[4].kind = MOBJ_K_USHIP;
    waves[0].objects[4].x_offset = FX_FromInt(30);
    waves[0].objects[4].y_offset = FX_FromInt(-20);
}

void Shooter::SpawnDespawnObjects()
{
    // Add any new MObjects that we need this frame.
    std::for_each(waves.begin(), waves.end(), [](LevelObjectWave wave)
    {
        if(levelStatus.scroll_units_travelled != wave.y_position) return;

        // We are at the Y position of this wave. Create the MObjects as required.
        for(int i=0; i<wave.numObjects; i++)
        {
            auto waveObj = wave.objects[i];
            UShip *mobj = static_cast<UShip *>(mobjectManager.Create(waveObj.kind));
            mobj->position.x = waveObj.x_offset + wave.x_position;
            mobj->position.y = waveObj.y_offset + 0;
            mobj->team = KateGL::TEAM_BAD;
            mobj->current_frame = 0;
            mobj->velocity.y = wave.speedFactor;
        }

        _64Drive_putstring("Wave found: creating an enemy\n");
    });

    // Destroy any objects that are offscreen and still moving away.
    std::for_each(mobjectManager.mobjects, mobjectManager.mobjects+MAX_MOBJECTS, [](MObjectsEntry &mobj)
    {
        if(mobj.mobject != NULL)
        {
            if(mobj.mobject->velocity.y > 0 && mobj.mobject->position.y > FX_FromInt(300))
            {
                // This MObject will never re-appear. Delete it.
                mobjectManager.Dispose(mobj.mobject);
            }
        }
    });
}

void Shooter::FrameLoop(display_context_t context)
{
    levelStatus.scroll_units_travelled += levelStatus.scroll_units_per_frame;

    // SpawnDespawnObjects();
    ProcessPlayerControls();

    mobjectManager.AdvanceAnimationVblankAll();
    mobjectManager.DoMovementAll();
    // BulletBehavior();

    // CheckBulletCollision();

    // Redraw objects.
    Bullet::DrawAll(context);
    UShip::DrawAll(context);

    // Draw the player.
    Talon *player = (Talon *)mobjectManager.mobjects[0].mobject;

    // Update the player's animation frame.
    if(playerLeftForFrames > 5)                                     player->current_frame = 1;
	else if(playerLeftForFrames > 0 && playerLeftForFrames <= 5)    player->current_frame = 2;
	else if(playerRightForFrames > 0 && playerRightForFrames <= 5)  player->current_frame = 3;
	else if(playerRightForFrames > 5)                               player->current_frame = 4;
	else                                                            player->current_frame = 0;
    Talon::ExecDisplayList(context, 0, false);

    char y_debug[20];
    sprintf(y_debug, "LEVEL Y: %05d", FX_ToInt(levelStatus.scroll_units_travelled));
    DL_WriteString(context, y_debug, TEX_F_KERNAL, 20, 190, 1, 1);
}