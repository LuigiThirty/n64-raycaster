#include <libdragon.h>
#include <kategl/fixed.h>
#include "mobj/kinds/_kinds.h"

typedef enum {
    NoRelativeVelocity,
    StraightDown,
    Sine,
} LevelObjectMovement;

typedef struct {
    MObjectKind kind;
    Fixed x_offset;
    Fixed y_offset;
} LevelObjectEntry;

typedef struct {
    Fixed x_position;               // The X position that's the center of this wave.
    Fixed y_position;               // The Y position that this wave will appear at.
    LevelObjectEntry objects[8];    // The objects that make up this wave.
    uint8_t numObjects;
    LevelObjectMovement movement;   // The movement type for this wave's MObjects.
    Fixed speedFactor;              // How fast do the MObjects animate?
} LevelObjectWave;