#ifndef SHOOTER_GAMESTATE_H
#define SHOOTER_GAMESTATE_H

#include <stdint.h>
#include <kategl/fixed.h>

extern int playerLeftForFrames;
extern int playerRightForFrames;

typedef struct {
    Fixed   scroll_units_travelled; // How many scroll units has the player travelled so far?
    Fixed   scroll_units_per_frame; // How many scroll units do we advance per frame?
} LevelStatus;

#endif