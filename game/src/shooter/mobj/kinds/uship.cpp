#include "shooter/mobj/kinds/uship.h"
#include "shooter/gamestate.h"

void UShip::DoMovement()
{

}

MObjectAnimationFrame UShip::_frames[16] = {
	ANIMATIONFRAME(TEX_USHIP1, 0, 0, 16, 19, 5),
	{ TEX_USHIP2, {.x = 0, .y = 0}, {.x = 16, .y = 19}, 5 },
	{ TEX_USHIP3, {.x = 0, .y = 0}, {.x = 16, .y = 19}, 5 },
	{ TEX_USHIP4, {.x = 0, .y = 0}, {.x = 16, .y = 19}, 5 },
	{ TEX_USHIP5, {.x = 0, .y = 0}, {.x = 16, .y = 19}, 5 },
	{ TEX_USHIP6, {.x = 0, .y = 0}, {.x = 16, .y = 19}, 5 },
	{ TEX_USHIP7, {.x = 0, .y = 0}, {.x = 16, .y = 19}, 5 },
	{ TEX_USHIP8, {.x = 0, .y = 0}, {.x = 16, .y = 19}, 5 },
};
uint8_t UShip::_number_of_frames = 8;

void UShip::ExecDisplayList(display_context_t context, int mobj_id, bool all)
{
	// Draw mobjects in groups of 5.
	mobjectManager.drawList.position = 0;
	auto remaining_count = mobjectManager.SetupQueue(MOBJ_K_USHIP);

	for(auto group=0; group < (remaining_count/5)+1; group++)
	{
		// Build a display list in the context given. Transfer it to the RDP once built.
		display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

		KateGL::DL::klPreamble(&display_list_current);
		KateGL::DL::klUseDisplayContext(&display_list_current, context);
		KateGL::DL::klPipeSync(&display_list_current);
		KateGL::DL::klSetupForMObject(&display_list_current);

		for(auto i=0; i<5; i++)
		{
			// End of the queue?
			if(mobjectManager.drawList.list[mobjectManager.drawList.position] == -1) break;

			UShip *mobj = mobjectManager.GetNextMObjectInDrawList<UShip>(MObjectList::ADVANCE_YES);
			KateGL::TextureListEntry *texture = KateGL::textureManager.GetTextureByName(mobj->frames[mobj->current_frame].texture_name);

			// Cache the texture and draw this object.
			if(KateGL::textureManager.GetCachedTexture() != texture)
			{
				KateGL::DL::klPipeSync(&display_list_current);
				display_list_current = KateGL::textureManager.CacheTexture(&display_list_current, texture);
			}

			KateGL::DL::klPipeSync(&display_list_current);
			KateGL::DL::klDrawTexturedMObject(&display_list_current, (KateGL::MObject *)mobj, texture);
		}

		KateGL::DL::klSyncAndEnd(&display_list_current);

		run_ucode();
		wait_for_rsp();
		wait_for_rdp();

		KateGL::textureManager.ClearCachedTexture();
	}
}

void UShip::DrawAll(display_context_t context)
{
	UShip::ExecDisplayList(context, 0, true);
}