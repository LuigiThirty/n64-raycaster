#ifndef MOBJ_USHIP_H
#define MOBJ_USHIP_H

#include <libdragon.h>
#include "kategl/kategl.hpp"
#include "shooter/mobj/manager.h"

class UShip : public KateGL::MObject, public ISteppable
{
    private:
    static MObjectAnimationFrame _frames[16];       // The list of animation frames for a MObject type.
    static uint8_t _number_of_frames;               // How many frames does this MObject type have?

    public:
    enum state_t {
        STATE_ALIVE,
        STATE_DYING,
        STATE_DEAD,
    };

    UShip() : ISteppable((MObjectAnimationFrame *)_frames, &UShip::_number_of_frames) {};

    void DoMovement();
    static void ExecDisplayList(display_context_t context, int mobj_id, bool all);
    static void DrawAll(display_context_t context);
};

#endif