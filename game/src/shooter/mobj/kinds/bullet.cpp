#include "shooter/mobj/kinds/bullet.h"

void Bullet::Fire(MObject *firing_object)
{
	position.x = firing_object->position.x + FX_FromInt(11);
	position.y = firing_object->position.y;
	position.z = firing_object->position.z;

	velocity.x = 0;
	velocity.y = FX_FromInt(-2);
	velocity.z = 0;

	team = firing_object->team;
	
	state = Bullet::STATE_MOVING;
}

void Bullet::ExecDisplayList(display_context_t context, int mobj_id, bool all)
{
	// display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

	// // Make sure that the RDP is idle before continuing.
	// KateGL::DL::klPreamble(&display_list_current);
	// KateGL::DL::klUseDisplayContext(&display_list_current, context);

	// rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_COPY|MODE_EN_TLUT|MODE_ALPHA_COMPARE_EN);
	// rdp_set_combine_mode(&display_list_current, CC_C0_RGB_MUL_ZERO_COLOR|CC_C1_RGB_MUL_ZERO_COLOR|CC_C0_RGB_ADD_TEXEL0_COLOR|CC_C1_RGB_ADD_TEXEL0_COLOR|
	// 	CC_C0_ALPHA_MUL_ZERO|CC_C0_ALPHA_ADD_TEXEL0|CC_C1_ALPHA_MUL_ZERO|CC_C1_ALPHA_ADD_TEXEL0);
	// rdp_sync(&display_list_current, SYNC_TILE);

	// rdp_set_env_color(&display_list_current, 0);
	// rdp_set_primitive_color(&display_list_current, 0);
	// rdp_set_blend_color(&display_list_current, 0);
	// rdp_set_fog_color(&display_list_current, 0);
	// rdp_set_fill_color(&display_list_current, 0);

	// KLBitmap *texture = KateGL::textureManager.GetTextureByName(TEX_BULLETS)->info;

	// rdp_sync(&display_list_current, SYNC_PIPE);
	// rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_COPY|MODE_ALPHA_COMPARE_EN);
	// rdp_load_texture(&display_list_current, TEXSLOT_0, 0x0, MIRROR_DISABLED, texture);
	// rdp_sync(&display_list_current, SYNC_TILE);
	// rdp_sync(&display_list_current, SYNC_LOAD);

	// if(all)
	// {
	// 	for(int i=0; i<MAX_MOBJECTS; i++)
	// 	{
	// 		if(mobjectManager.mobjects[i].kind == MOBJ_K_BULLET)
	// 		{
	// 			MObject *bullet = mobjectManager.mobjects[i].mobject;
	// 			rdp_draw_textured_rectangle_scaled(&display_list_current, TEXSLOT_0, 
	// 				FX_ToInt(bullet->position.x), FX_ToInt(bullet->position.y), 
	// 				FX_ToInt(bullet->position.x)+texture->size.x-1, FX_ToInt(bullet->position.y)+texture->size.y-1, 
	// 				0.25, 1, 
	// 				0, 0);
	// 			rdp_sync(&display_list_current, SYNC_PIPE);
	// 		}
	// 	}
	// }
	// else
	// {
	// 	MObject *bullet = mobjectManager.mobjects[mobj_id].mobject;
	// 	rdp_draw_textured_rectangle_scaled(&display_list_current, TEXSLOT_0, 
	// 		FX_ToInt(bullet->position.x), FX_ToInt(bullet->position.y), 
	// 		FX_ToInt(bullet->position.x)+texture->size.x-1, FX_ToInt(bullet->position.y)+texture->size.y-1, 
	// 		0.25, 1, 
	// 		0, 0);
	// 	rdp_sync(&display_list_current, SYNC_PIPE);
	// }

	// rdp_sync(&display_list_current, SYNC_FULL);
	// rdp_end_display_list(&display_list_current);

	// run_ucode();
	// wait_for_rsp();
	// wait_for_rdp();

	// Draw mobjects in groups of 8.
	mobjectManager.drawList.position = 0;
	auto remaining_count = mobjectManager.SetupQueue(MOBJ_K_BULLET);

	KateGL::TextureListEntry *texture = KateGL::textureManager.GetTextureByName(TEX_BULLETS);

	for(auto group=0; group < (remaining_count/8)+1; group++)
	{
		// Build a display list in the context given. Transfer it to the RDP once built.
		display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

		KateGL::DL::klPreamble(&display_list_current);
		KateGL::DL::klUseDisplayContext(&display_list_current, context);
		KateGL::DL::klPipeSync(&display_list_current);

		rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_COPY|MODE_ALPHA_COMPARE_EN);
		rdp_set_combine_mode(&display_list_current, CC_C0_RGB_MUL_ZERO_COLOR|CC_C1_RGB_MUL_ZERO_COLOR|CC_C0_RGB_ADD_TEXEL0_COLOR|CC_C1_RGB_ADD_TEXEL0_COLOR|
			CC_C0_ALPHA_MUL_ZERO|CC_C0_ALPHA_ADD_TEXEL0|CC_C1_ALPHA_MUL_ZERO|CC_C1_ALPHA_ADD_TEXEL0);
		rdp_sync(&display_list_current, SYNC_TILE);

		for(auto i=0; i<8; i++)
		{
			// End of the queue?
			if(mobjectManager.drawList.list[mobjectManager.drawList.position] == -1) break;

			auto *mobj = mobjectManager.GetNextMObjectInDrawList<Bullet>(MObjectList::ADVANCE_YES);

			// Cache the texture and draw this object.
			if(KateGL::textureManager.GetCachedTexture() != texture)
			{
				KateGL::DL::klPipeSync(&display_list_current);
				display_list_current = KateGL::textureManager.CacheTexture(&display_list_current, texture);
			}

			KateGL::DL::klPipeSync(&display_list_current);
			rdp_draw_textured_rectangle_scaled(&display_list_current, TEXSLOT_0, 
				FX_ToInt(mobj->position.x), FX_ToInt(mobj->position.y), 
				FX_ToInt(mobj->position.x)+texture->info->size.x-1, FX_ToInt(mobj->position.y)+texture->info->size.y-1, 
				0.25, 1, 
				0, 0);
		}

		KateGL::DL::klSyncAndEnd(&display_list_current);

		run_ucode();
		wait_for_rsp();
		wait_for_rdp();
	}
}

void Bullet::DrawAll(display_context_t context)
{
	Bullet::ExecDisplayList(context, 0, true);
}