#ifndef MOBJ_TALON_H
#define MOBJ_TALON_H

#include <libdragon.h>
#include "kategl/kategl.hpp"
#include "shooter/mobj/manager.h"

class Talon : public KateGL::MObject, public ISteppable
{
    private:
    static MObjectAnimationFrame _frames[16];       // The list of animation frames for a MObject type.
    static uint8_t _number_of_frames;               // How many frames does this MObject type have?

    public:
    enum state_t {
        STATE_ALIVE,
        STATE_DYING,
        STATE_DEAD,
    };

    void DoMovement();
    void SetAnimationFrame();

    Talon() : ISteppable((MObjectAnimationFrame *)_frames, &Talon::_number_of_frames) {};

    static void ExecDisplayList(display_context_t context, int mobj_id, bool all);
    static void DrawAll(display_context_t context);
};

#endif