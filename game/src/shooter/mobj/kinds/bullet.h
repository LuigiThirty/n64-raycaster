#ifndef BULLET_H
#define BULLET_H

#include <libdragon.h>
#include "kategl/kategl.hpp"
#include "shooter/mobj/manager.h"

class Bullet : public KateGL::MObject
{
    public:
    enum state_t {
        STATE_MOVING
    };

    void Fire(MObject *firing_object);
    void DoMovement();
    
    static void ExecDisplayList(display_context_t context, int mobj_id, bool all);
    static void DrawAll(display_context_t context);
};

#endif