#include "shooter/mobj/kinds/talon.h"
#include "shooter/gamestate.h"

MObjectAnimationFrame Talon::_frames[16] =
{
	{ TEX_TALON, 	{.x = 0, .y = 0 }, {.x = 32, .y = 25 } },
	{ TEX_TALONLL, 	{.x = 0, .y = 0 }, {.x = 32, .y = 25 } },
	{ TEX_TALONL, 	{.x = 0, .y = 0 }, {.x = 32, .y = 25 } },
	{ TEX_TALONR, 	{.x = 0, .y = 0 }, {.x = 32, .y = 25 } },
	{ TEX_TALONRR, 	{.x = 0, .y = 0 }, {.x = 32, .y = 25 } },
};
uint8_t Talon::_number_of_frames = 5;

void Talon::ExecDisplayList(display_context_t context, int mobj_id, bool all)
{
// Build a display list in the context given. Transfer it to the RDP once built.
	display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

	Talon *mobj = (Talon *)mobjectManager.mobjects[mobj_id].mobject;
	
	// Make sure that the RDP is idle before continuing.
	KateGL::DL::klPreamble(&display_list_current);
	KateGL::DL::klPipeSync(&display_list_current);

	KateGL::DL::klUseDisplayContext(&display_list_current, context);
	KateGL::DL::klSetupForMObject(&display_list_current);

	KateGL::TextureListEntry *texture = KateGL::textureManager.GetTextureByName(mobj->frames[mobj->current_frame].texture_name);
	if(KateGL::textureManager.GetCachedTexture() != texture)
	{
		KateGL::DL::klPipeSync(&display_list_current);
		display_list_current = KateGL::textureManager.CacheTexture(&display_list_current, texture);
	}

	KateGL::DL::klPipeSync(&display_list_current);
	KateGL::DL::klDrawTexturedMObject(&display_list_current, (KateGL::MObject *)mobj, texture);

	KateGL::DL::klSyncAndEnd(&display_list_current);

	run_ucode();
	wait_for_rsp();
	wait_for_rdp();

	KateGL::textureManager.ClearCachedTexture();
}

void Talon::DrawAll(display_context_t context)
{
	Talon::ExecDisplayList(context, 0, true);
}