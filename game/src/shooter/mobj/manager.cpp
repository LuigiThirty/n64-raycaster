#include "shooter/mobj/manager.h"
#include "shooter/mobj/kinds/_kinds.h"
#include "string.h"
#include "64drive.h"

#include "stdlib.h"
#include "stdio.h"

#include "assets/textures/_identifiers.h"
#include "kategl/kategl.hpp"

#include <algorithm.h>

MObjectManager mobjectManager;

// For fast access.
KateGL::MObject *PLAYER_MOBJECT;

void MObjectManager::Initialize()
{
    drawList.list.fill(-1);
    drawList.position = 0;
}


KateGL::MObject * MObjectManager::Create(MObjectKind kind)
{
    // Find the first free MObject.
    for(int i=0;i<MAX_MOBJECTS;i++)
    {
        if(mobjects[i].kind == MOBJ_K_NULL)
        {
            sprintf(dbg, "MObjectManager::Create: found a free object at id %d\n", i);
            _64Drive_putstring(dbg);

            switch(kind)
            {
                case MOBJ_K_BULLET: mobjects[i].mobject = static_cast<KateGL::MObject *>(new Bullet); break;
                case MOBJ_K_PLAYER: mobjects[i].mobject = static_cast<KateGL::MObject *>(new Talon); break;
                case MOBJ_K_USHIP: mobjects[i].mobject = static_cast<KateGL::MObject *>(new UShip); break;
                default:
                sprintf(dbg, "*** FATAL MObjectManager: No Dispose entry for kind %d\n", kind);
                _64Drive_putstring(dbg);
                while(true) {};
                break;
            }
            mobjects[i].mobject->SetID(i);
            mobjects[i].kind = kind;
            return mobjects[i].mobject;
        }
    }

    _64Drive_putstring("*** FATAL: MObjectManager has no free MObjects\n");
    while(true) {};
}

/*  
 *  Dispose of the specified MObject. 
 *  Change its tag to NULL and remove the pointer, then free it.
 */
void MObjectManager::Dispose(KateGL::MObject *mobj)
{
    int id = mobj->GetID();
    sprintf(dbg, "Disposing of MObject %p with ID %d\n", mobj, mobj->GetID());
    _64Drive_putstring(dbg);

    switch(mobjects[mobj->GetID()].kind)
    {
        case MOBJ_K_BULLET: delete static_cast<Bullet *>(mobjects[id].mobject); break;
        case MOBJ_K_PLAYER: delete static_cast<Talon *>(mobjects[id].mobject); break;
        case MOBJ_K_USHIP:  delete static_cast<UShip *>(mobjects[id].mobject); break;
        default:
        sprintf(dbg, "*** FATAL MObjectManager: No Dispose entry for kind %d\n", mobjects[mobj->GetID()].kind);
        _64Drive_putstring(dbg);
        while(true) {};
        break;
    }

    mobjects[id].kind = MOBJ_K_NULL;
    mobjects[id].mobject = NULL;
}

int MObjectManager::Count(MObjectKind kind)
{
    auto numfound = 0;

    std::for_each(mobjects, mobjects+MAX_MOBJECTS, [&](auto &mobj)
    {
        if(mobj.kind == kind) numfound++;
    });

    return numfound;
}

void MObjectManager::DoMovementAll()
{
    std::for_each(mobjects, mobjects+MAX_MOBJECTS, [&](auto &mobj)
    {
        if(mobj.kind != MOBJ_K_NULL)
        {
            mobj.mobject->DoMovement();
        }
    });
}

/* Set up the draw queue with one mobj_id. */
int MObjectManager::SetupQueue(int mobj_id)
{
    drawList.list[0] = mobj_id;
    drawList.list[1] = -1;
    return 1;
}

/* Set up the draw queue with all mobjs of a specific MObjectKind. */
int MObjectManager::SetupQueue(MObjectKind kind)
{
    auto count = 0;

    std::for_each(mobjects, mobjects+MAX_MOBJECTS, [&, i = int(0)](auto &mobj) mutable
    {
        if(mobj.kind == kind) drawList.list[count++] = i;
        i++;
    });

    drawList.list[count] = -1;

    return count;
}

bool MObjectManager::IsMObjectAvailableInQueue()
{
    return drawList.list[drawList.position] != -1;
}

void MObjectManager::AdvanceAnimationVblankAll()
{
    std::for_each(mobjects, mobjects+MAX_MOBJECTS, [](auto &mobj)
    {
        switch(mobj.kind)
        {
            case MOBJ_K_PLAYER: static_cast<Talon *>(mobj.mobject)->AdvanceAnimationVblank(); break;
            case MOBJ_K_USHIP:  static_cast<UShip *>(mobj.mobject)->AdvanceAnimationVblank(); break;
            case MOBJ_K_BULLET:
            case MOBJ_K_NULL:
                break;
        }
    });
}

