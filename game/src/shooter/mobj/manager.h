#ifndef SHOOTER_OBJECTS_H
#define SHOOTER_OBJECTS_H

#include <array.h>
#include <algorithm.h>

// #include "rcp/sync.h"
#include "kategl/kategl.hpp"
#include "shooter/objkinds.h"
#include "assets/textures/_identifiers.h"
#include "microcode/basic.h"
#include "shooter/mobj/interfaces/steppable.h"

extern "C" void __cxa_pure_virtual();

class MObjectManager;

/* An entry in the mobjects table. */
struct MObjectsEntry {
    // Tagged pointer to a MObject, more or less.
    KateGL::MObject *mobject;
    MObjectKind kind;
};

typedef struct {
    int queue[MAX_MOBJECTS];
    int position;
    std::array<int, MAX_MOBJECTS> list;

    typedef enum {
        ADVANCE_NO = false,
        ADVANCE_YES = true
    } QueueAdvance;
} MObjectList;

/* The motion object manager. Handles allocation and deallocation of MObjs. */
class MObjectManager {
    public:
    MObjectsEntry mobjects[MAX_MOBJECTS];
    MObjectList drawList;

    void Initialize();
    int Count(MObjectKind kind);

    int SetupQueue(int mobj_id);
    int SetupQueue(MObjectKind kind);
    bool IsMObjectAvailableInQueue();

    void DoMovementAll();
    void AdvanceAnimationVblankAll();

    KateGL::MObject *Create(MObjectKind kind);
    void Dispose(KateGL::MObject *mobj);

    template <class T> 
    T* GetNextMObjectInDrawList(bool advance=false)
    {
        if(advance)
        {
            return static_cast<T*>(mobjects[drawList.list[drawList.position++]].mobject);
        }
        else
        {
            return static_cast<T*>(mobjects[drawList.list[drawList.position]].mobject);
        }
    }
};

extern MObjectManager mobjectManager;
extern KateGL::MObject *PLAYER_MOBJECT;

#endif