#ifndef STEPPABLE_H
#define STEPPABLE_H

static char dbg[512];

#define ANIMATIONFRAME(name, offsetX, offsetY, sizeX, sizeY, duration) { name, {.x = offsetX, .y = offsetY}, {.x = sizeX, .y = sizeY}, duration }

typedef struct 
{
    char texture_name[10];
    Point2D offset;         // Offset on the texture sheet.
    Point2D size;           // Size of the animation frame in pixels.
    int8_t duration;        // Duration of the animation in frames. -1: Forever.
} MObjectAnimationFrame;

class ISteppable 
{
    public:
    // Animation.
    int current_frame = 0;                  // The current animation frame this object is on.
    int duration_remaining = 0;             // How many VBLANKs until advancing to the next animation frame?
    MObjectAnimationFrame *frames;   // The list of animation frames for a MObject type.
    uint8_t number_of_frames;        // How many frames does this MObject type have?

    ISteppable(MObjectAnimationFrame *_frames, uint8_t *_number_of_frames)
    {
        frames = _frames;
        number_of_frames = *_number_of_frames;
    }

    void AdvanceAnimationVblank()
    {
        // sprintf(dbg, "AdvanceAnimationVblank. %d frames\n", number_of_frames);
        // _64Drive_putstring(dbg);

        if(duration_remaining == 0)
        {
            current_frame = (current_frame + 1) % number_of_frames;
            duration_remaining = frames[current_frame].duration;
        }
        else if(duration_remaining != -1) // If it's a FOREVER frame, never decrement the counter.
        {
            duration_remaining--;
        }
    }
};

#endif