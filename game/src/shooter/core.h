#ifndef CORE_H
#define CORE_H

#include <libdragon.h>
#include "kategl/kategl.hpp"
#include "shooter/mobj/manager.h"
#include "shooter/gamestate.h"

extern int playerLeftForFrames;
extern int playerRightForFrames;

class Shooter {
    public:
    void Setup();
    void FrameLoop(display_context_t context);

    void ProcessPlayerControls();
    void FireBullet(KateGL::MObject *firing_object);
    void BulletBehavior();
    void CheckBulletCollision();
    void SpawnDespawnObjects();
};

#endif