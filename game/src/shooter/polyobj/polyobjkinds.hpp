#pragma once

// key
typedef enum polyobject_kind_t
{
    POLYOBJ_K_NULL,     // 0
    POLYOBJ_K_CUBE,     // 1
    POLYOBJ_KINDS_NUM   // always at the end!
} PolyObjectKind;
