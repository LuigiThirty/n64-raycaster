#pragma once

#include <array.h>
#include <algorithm.h>

#include "kategl/kategl.hpp"
#include "shooter/polyobj/polyobjkinds.hpp"

#define MAX_POLYOBJECTS 256

/* An entry in the mobjects table. */
struct PolyObjectsEntry {
    // Tagged pointer to a PolyObject, more or less.
    KateGL::PolyObject *polyobject;
    PolyObjectKind kind;
};

typedef struct {
    int queue[MAX_POLYOBJECTS];
    int position;
    std::array<int, MAX_POLYOBJECTS> list;

    typedef enum {
        ADVANCE_NO = false,
        ADVANCE_YES = true
    } QueueAdvance;
} PolyObjectList;

class PolyObjectManager {
    public:
    PolyObjectsEntry polyobjects[MAX_POLYOBJECTS];
    PolyObjectList drawList;

    void Initialize();
    int Count(PolyObjectKind kind);

    KateGL::PolyObject * PolyObjectManager::Create(PolyObjectKind kind);
    void Dispose(KateGL::PolyObject *polyobj);
};