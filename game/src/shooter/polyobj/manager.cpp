#include "polyobj/manager.hpp"

#include "polyobj/kinds/cube.hpp"

PolyObjectManager polyObjectManager;

void PolyObjectManager::Initialize()
{
    drawList.list.fill(-1);
    drawList.position = 0;
}

int PolyObjectManager::Count(PolyObjectKind kind)
{
    auto numfound = 0;

    std::for_each(polyobjects, polyobjects+MAX_POLYOBJECTS, [&](auto &polyobj)
    {
        if(polyobj.kind == kind) numfound++;
    });

    return numfound;
}

KateGL::PolyObject * PolyObjectManager::Create(PolyObjectKind kind)
{
    // Find the first free MObject.
    for(int i=0;i<MAX_MOBJECTS;i++)
    {
        if(polyobjects[i].kind == POLYOBJ_K_NULL)
        {
            sprintf(dbg, "PolyObjectManager::Create: found a free object at id %d\n", i);
            _64Drive_putstring(dbg);

            switch(kind)
            {
                case POLYOBJ_K_CUBE: polyobjects[i].polyobject = static_cast<KateGL::PolyObject *>(new Cube); break;
                default:
                sprintf(dbg, "*** FATAL PolyObjectManager: No Create entry for kind %d\n", kind);
                _64Drive_putstring(dbg);
                while(true) {};
                break;
            }
            polyobjects[i].polyobject->SetID(i);
            polyobjects[i].kind = kind;
            return polyobjects[i].polyobject;
        }
    }

    _64Drive_putstring("*** FATAL: MObjectManager has no free MObjects\n");
    while(true) {};
}

/*  
 *  Dispose of the specified MObject. 
 *  Change its tag to NULL and remove the pointer, then free it.
 */
void PolyObjectManager::Dispose(KateGL::PolyObject *polyobj)
{
    int id = polyobj->GetID();
    sprintf(dbg, "Disposing of PolyObject %p with ID %d\n", polyobj, polyobj->GetID());
    _64Drive_putstring(dbg);

    switch(polyobjects[polyobj->GetID()].kind)
    {
        case POLYOBJ_K_CUBE: delete static_cast<Cube *>(polyobjects[id].polyobject); break;
        default:
        sprintf(dbg, "*** FATAL PolyObjectManager: No Dispose entry for kind %d\n", polyobjects[polyobj->GetID()].kind);
        _64Drive_putstring(dbg);
        while(true) {};
        break;
    }

    polyobjects[id].kind = POLYOBJ_K_CUBE;
    polyobjects[id].polyobject = NULL;
}