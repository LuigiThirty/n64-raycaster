#pragma once

#include <libdragon.h>
#include "kategl/kategl.hpp"
#include "shooter/polyobj/manager.hpp"
#include "assets/models/cube.hpp"

class Cube : public KateGL::PolyObject
{
    public:
    enum state_t {
        STATE_NULL,
    };

    Cube()
    {
	    mesh = new KateGL::Mesh(cube, cubeVertexCount, cubeTriangles, cubeTriangleCount);
    }
};
