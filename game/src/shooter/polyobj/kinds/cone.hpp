#pragma once

#include <libdragon.h>
#include "kategl/kategl.hpp"
#include "shooter/polyobj/manager.hpp"
#include "assets/models/cone.hpp"

class Cone : public KateGL::PolyObject
{
    public:
    enum state_t {
        STATE_NULL,
    };

    Cone()
   {
	    mesh = new KateGL::Mesh(cone, coneVertexCount, coneTriangles, coneTriangleCount);
    }
};
