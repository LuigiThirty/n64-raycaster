#ifndef OBJKINDS_H
#define OBJKINDS_H

#include "assets/textures/_identifiers.h"

// key
typedef enum mobject_kind_t
{
    MOBJ_K_NULL,        // 0
    MOBJ_K_PLAYER,      // 1
    MOBJ_K_BULLET,      // 2
    MOBJ_K_USHIP,       // 3

    MOBJ_KINDS_NUM      // always at the end!
} MObjectKind;

static const char MOBJ_DEFAULT_TEXTURE[10][10] =
{
    "NULL",
    "TALON",
    "BULLETS",
    "USHIP",
};

#endif