#include "pak.hpp"

    // Load a pak file into RDRAM from DragonFS.
    void Pak::LoadFromDFS(char *pak_filename)
    {
        char buf[128];

        sprintf(buf, "attempting to load %s from DFS\n", pak_filename);
        _64Drive_putstring(buf);

        auto handle = dfs_open(pak_filename);

        sprintf(buf, "handle is %d\n", handle);
        _64Drive_putstring(buf);

        auto size = dfs_size(handle);

        data = (uint8_t *)malloc(size);
        dfs_read(data, 1, size, handle);
        dfs_close(handle);

        header.id = ((uint32_t *)data)[0]; 
        header.file_table_offset = SwapEndiannessUInt32(((uint32_t *)data)[1]);
        header.file_table_size = SwapEndiannessUInt32(((uint32_t *)data)[2]);

        sprintf(buf, "%s contains %08X files\n", pak_filename, header.file_table_size / sizeof(FileRecord));
        _64Drive_putstring(buf);
        
    }

    uint8_t *Pak::GetFileDataPtr(char *filename)
    {
        // TODO: error out if we try to perform this on a NULL pak
        uint8_t *directory = data + header.file_table_offset;
        for(int i=0; i<header.file_table_size / 64; i++)
        {
            FileRecord *rec = (FileRecord *)directory;
            char buf[128];

            if(memcmp(filename, rec->name, 8) == 0)
            {
                sprintf(buf, "file is %s. offset %08X. file length %d\n", rec->name, SwapEndiannessUInt32(rec->starting_offset), SwapEndiannessUInt32(rec->length));
                _64Drive_putstring(buf);

                return data + SwapEndiannessUInt32(rec->starting_offset);
            }
            
            directory += sizeof(FileRecord);
        }

        return NULL;
    }

    uint32_t Pak::GetFileSize(char *filename)
    {
        uint8_t *directory = data + header.file_table_offset;
        for(int i=0; i<header.file_table_size / 64; i++)
        {
            FileRecord *rec = (FileRecord *)directory;

            if(memcmp(filename, rec->name, 8) == 0)
            {
                return SwapEndiannessUInt32(rec->length);
            }
            
            directory += sizeof(FileRecord);
        }

        return -1;
    }

    void Pak::Unload()
    {
        free(data);
    }