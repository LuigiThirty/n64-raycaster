#define R_InputCommandsPtr $k0
#define R_RDPBufferPtr     $k1

#define V_ELEMENT0      8
#define V_ELEMENT1      9
#define V_ELEMENT2      10
#define V_ELEMENT3      11
#define V_ELEMENT4      12
#define V_ELEMENT5      13
#define V_ELEMENT6      14
#define V_ELEMENT7      15

# Fixed point masks
#define FIXED_S11DOT2 0x0FFFC000

# Vertex struct
    .set VERTEX_X, 0
    .set VERTEX_Y, 4
    .set VERTEX_Z, 8
    .set VERTEX_W, 12
    .set VERTEX_F, 16
    .set VERTEX_S, 20
    .set VERTEX_T, 24
    .set VERTEX_R, 28
    .set VERTEX_G, 32
    .set VERTEX_B, 36
    .set VERTEX_A, 40
    .set VERTEX_Size, 44

    .set        at
    #include "macros.i"