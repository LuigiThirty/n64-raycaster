    #include <ucode.S>

#define R_InputCommandsPtr $k0
#define R_RDPBufferPtr     $k1

# Functions to load for matrix functions.
#
# There are two Matrix Operands, A and B. They are stored in VEC0-VEC3 and VEC4-VEC7.
# Matrix Operands are in the Q16.16 format.
# Integer rows are stored in  A: VEC0-VEC1 B: VEC2-VEC3 R: VEC4-VEC5
# Fraction rows are stored in A: VEC6-VEC7 B: VEC8-VEC9 R: VEC10-VEC11

    .section .overlay1_text

# The RSP datasheet implies you need a different vector register for integers and fractions.
LoadMatrixOpA:
	# Load a matrix into VEC0/VEC6. Effective address is 0($v0).
        # Integers
        lqv         VEC0, 0, 0, S_A0        # VEC0 <- 0(s8)
        lqv         VEC1, 0, 1, S_A0        # VEC1 <- 16(s8)
        # Fractions
        lqv         VEC6, 0, 2, S_A0        # VEC6 <- 24(s8)
        lqv         VEC7, 0, 3, S_A0        # VEC7 <- 32(s8)

	jr          $ra
	nop

LoadMatrixOpB:
        # Load a matrix into VEC2/VEC8.
        # Integers
        lqv         VEC2, 0, 0, S_A0        # VEC4 <- 0(s8)
        lqv         VEC3, 0, 1, S_A0        # VEC5 <- 16(s8)
        # Fractions
        lqv         VEC8, 0, 2, S_A0        # VEC8 <- 24(s8)
        lqv         VEC9, 0, 3, S_A0        # VEC9 <- 32(s8)
        
	jr          $ra
	nop
        
##########################
        # Matrix math functions

MatrixAB_Add:        
        # Add Matrix A to Matrix B, storing the result in Matrix R.
        vaddc       VEC10, VEC6, VEC8, 0
        vadd        VEC4, VEC0, VEC2, 0

        vaddc       VEC11, VEC7, VEC9, 0
        vadd        VEC5, VEC1, VEC3, 0
        
        jr          $ra
        nop
                           
MatrixAB_Sub:        
        # Add Matrix A to Matrix B, storing the result in Matrix R.
        vsubc       VEC10, VEC6, VEC8, 0
        vsub        VEC4, VEC0, VEC2, 0

        vsubc       VEC11, VEC7, VEC9, 0
        vsub        VEC5, VEC1, VEC3, 0
        
        jr          $ra
        nop

MatrixAB_Multiply:
        jr      $ra
        nop

DL_MVPMultiply:
        # Model * View * Projection

        # Set the transformation to the identity matrix.
        li      $t0, 0x0001
        li      $t1, MatrixTransformInt
        li      $t2, 30
        li      $t3, 0x0002
        li      $t4, 0x0003
        li      $t5, 0x0004

        sh      $t0, 0($t1)
        sh      $t3, 2($t1)
        sh      $t4, 4($t1)
        sh      $t5, 6($t1)

        sh      $zero, 8($t1)
        sh      $t0, 10($t1)
        sh      $zero, 12($t1)
        sh      $zero, 14($t1)

        sh      $zero, 16($t1)
        sh      $zero, 18($t1)
        sh      $t0, 20($t1)
        sh      $zero, 22($t1)

        sh      $zero, 24($t1)
        sh      $zero, 26($t1)
        sh      $zero, 28($t1)
        sh      $t0, 30($t1)

        PUSH    $ra

        # Projection * View
        #li      $a0, MatrixTransform
        li      $a0, MatrixProjection
        jal     LoadMatrixOpA
        nop

        li      $a0, MatrixProjection
        jal     LoadMatrixOpB
        nop     

        jal     MatrixAB_Multiply
        nop

        # VEC04-05 is transform.int
        # VEC10-11 is transform.frac
        li      $a0, MatrixTransformInt
        sqv     VEC4, 0, 0, S_A0
        srv     VEC4, 0, 0, S_A0
        sqv     VEC5, 0, 1, S_A0
        srv     VEC5, 0, 1, S_A0

        li      $a0, MatrixTransformFrac
        sqv     VEC10, 0, 0, S_A0
        srv     VEC10, 0, 0, S_A0
        sqv     VEC11, 0, 1, S_A0
        srv     VEC11, 0, 1, S_A0

9:
        POP     $ra
        addi    R_InputCommandsPtr, 8   # advance 2 words                        
        
        jr      $ra                     # done
        nop                             # delay slot

        .data

# Operand A storage
MatrixA:
MatrixAInt:
        .space      32
MatrixAFrac:
        .space      32

        # Operand B storage
MatrixB:
MatrixBInt:
        .space      32
MatrixBFrac:
        .space      32

        # Result storage
MatrixR:
MatrixRInt:
        .space      32
MatrixRFrac:
        .space      32