        .org 0x800

# Overlay addresses.
OVERLAY_Boot:   .word 0
OVERLAY_Vertex: .word 0

LoadedSegmentAddress:   .word 0

EndOfDisplayList:       .word 0

# All this must be 64-bit aligned!
# Projection/View matrices
MatrixProjection:
MatrixProjectionInt:    .space 32           
MatrixProjectionFrac:   .space 32

MatrixView:
MatrixViewInt:          .space 32
MatrixViewFrac:         .space 32

MatrixModel:
MatrixModelInt:         .space 32
MatrixModelFrac:        .space 32

        # Final transformation matrix
MatrixTransform:
MatrixTransformInt:     .space 32
MatrixTransformFrac:    .space 32
        
        # Scratch memory, word-aligned
        .align 4        # 16-byte alignment
TempWord1:              .word   0
TempWord2:              .word   0
TempDMA:                .space  64

TempDivisor:    .word 0
TempDividend:   .word 0

        # Draw buffer

        .align 4        # 16-byte alignment
TempVertex0_XYZW:       .space 16
TempVertex1_XYZW:       .space 16
TempVertex2_XYZW:       .space 16

TempVertex0_RGBA:       .space 4
TempVertex1_RGBA:       .space 4
TempVertex2_RGBA:       .space 4

TempVertex0_NonNormZ:   .space 4
TempVertex1_NonNormZ:   .space 4
TempVertex2_NonNormZ:   .space 4

        # Working buffers for triangle processing.
CoefficientYh: .word 0
CoefficientYm: .word 0
CoefficientYl: .word 0                                        
CoefficientXh: .word 0
CoefficientXm: .word 0
CoefficientXl: .word 0
SlopeDXhDy: .word 0
SlopeDXmDy: .word 0
SlopeDXlDy: .word 0

TriangleCommand: .word 0

RSPSemaphore: .word 0

Test0:  .word 0
Test1:  .word 0
Test2:  .word 0
Test3:  .word 0
