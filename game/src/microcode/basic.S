        #include <ucode.S>
        
        #include "n64.inc"
        
                    /* MIPS register assignments
        0           zero    - always 0          
        1           $at     - reserved by the assembler
        2 to 3      $v0-$v1 - expression/function results
        4 to 7      $a0-$a3 - First four parameters of a subroutine
        8 to 15     $t0-$t7 - Available for subroutines, caller-saved
        16 to 23    $s0-$s7 - Callee-saved, preserve in a subroutine if modified
        24 to 25    $t8-$t9 - Available for subroutines, caller-saved
        26 to 27    $k0-$k1 - Interrupt/trap handler. RCP does not have one, so available.
        28          $gp - Points to static data segment. Unused in the RCP, available.
        29          $sp - Stack pointer
        30          $fp - Frame pointer
        31          $ra - Function return address

                    ucode.S provides preprocessor macros for the RSP vector registers
                    they are defined as v0-v31
                    see RSP manual for instruction set and element layout
                    */

        .section .text

_start:        
        # Initialize SP
        li      $sp, 0x1000

        sw      $zero, LoadedSegmentAddress

        li      $t0, 0x16
        mtc0    $t0, $11        # use XBUS DMEM DMA, initialize RDP STATUS

        mfc0    $t0, $7         # set semaphore bit

        # jal     EraseDisplayList
        # nop

        # Set CMD_START to 0x10
        li      $t0, 0x10
        mtc0    $t0, $8      # CMD_START

        # Process a display list, one command at a time.
        jal         ProcessDisplayList
        nop

        sw      R_RDPBufferPtr, EndOfDisplayList
        
_endprogram:
        # clean up the buffer at 0x400-0x7FF
        li      $t0, 0x400
        li      $t1, 256
1:
        sw      $zero, ($t0)
        addi    $t0, 4
        addi    $t1, -1
        bgtz    $t1, 1b
        nop

        mtc0    $zero, $7       # clear semaphore bit

Break:
        break
        nop

ProcessDisplayList:
        PUSH        $ra
        li          R_InputCommandsPtr, InputDisplayList # use $k0 for the display list offset
        li          R_RDPBufferPtr, 0x10

dlProcessingLoop:
        lw          $s0, (R_InputCommandsPtr)
        bgez        $s0, dlProcessingComplete # if b31 is not set, end processing
        nop
        
        srl         $s1, $s0, 24        # grab the high byte of $s0, put it into $s1       
        andi        $s1, $s1, 0x7F      # mask off high bit
        sll         $s1, $s1, 2         # $s0 = index*4
        lw          $s2, DisplayListJumpTable($s1) # get the address from the jump table        
        jalr        $s2                 # jump to the address in the table
        nop

        # $k1 is the end of the ring buffer. it starts at $0
        mtc0    R_RDPBufferPtr, $9  # Update CMD_END

        # Is the buffer past 0x300?
        li      $t1, 0x300
        blt     R_RDPBufferPtr, $t1, 9f	# if $t0 < $t1 then target

        PUSH    $ra
        jal     ResetFIFO
        nop
        POP     $ra

9:
        j           dlProcessingLoop
        nop
        
dlProcessingComplete:
        POP         $ra        
        jr          $ra                 
        nop

###            
ResetFIFO:
        # Make sure that we're not currently reading from the FIFO.
1:
        mfc0    $t0, $13
        andi    $t0, $t0, 0x400 # CMD_START is valid
        bne     $t0, $zero, 1b  # wait for CMD_START to be invalid

2:
        # Make sure that RDP_CURRENT == RDP_END before continuing!
        mfc0    $t0, $9
        mfc0    $t1, $10
        bne     $t0, $t1, 2b

        # Halt until these are both valid...
        li      $t0, 0x08       # set RDP HALT
        mtc0    $t0, $11        # set RDP HALT

        # Set CMD_START and CMD_END to 0x10
        li      $t0, 0x10
        mtc0    $t0, $8  # CMD_START
        mtc0    $t0, $9  # CMD_END

        li      $t0, 0x04       # clear RDP HALT
        mtc0    $t0, $11        # clear RDP HALT

        li      R_RDPBufferPtr, 0x10

        jr      $ra
        nop

        break

###########################

##########################
        # Q16 fixed point routines. All routines handle signed numbers.
Q16_Add:
        # Add two Q16 numbers.
        # $v0 <- $a0+$a1 
        add         $v0,$a1,$a2
        jr          $ra
        nop         # branch delay slot

Q16_Sub:
        # Subtract two Q16 numbers.
        # $v0 <- $a0-$a1
        sub         $v0,$a1,$a2
        jr          $ra
        nop         # branch delay slot

#######################################
# Display list commands

DL_SetOtherModes:
DL_SetFillColor:
DL_SetColorImage:
DL_SetBlendColor:
DL_SetEnvColor:
DL_SetFogColor:
DL_SetPrimColor:
DL_SetCombineMode:
DL_SetScissor:
DL_NOP: 
DL_CopyTwoWords:
DL_SyncTile:
DL_SyncPipe:
DL_SyncFull:
DL_SyncLoad:
DL_FillRectangle:
DL_LoadTile:
DL_LoadBlock:
DL_SetPrimDepth:
DL_SetTile:
DL_SetTileSize:
DL_SetZImage:
DL_SetTextureImage:
DL_LoadTlut:
        # Copy two words straight to the end of the display list.
        # No processing is required.
        # We assume that the CPU has set these functions up correctly.
        li      $t1, 2
        b       DL_CopyFromCPU


DL_TextureRectangle:
        # Texture rectangle is copied from the CPU but is four words, not two.
        li      $t1, 4
        b       DL_CopyFromCPU

DL_CopyFromCPU:
1:      # Copy $t1 32-bit words from (R_InputCommandsPtr) to (R_RDPBufferPtr).
        lw      $t0, (R_InputCommandsPtr)
        sw      $t0, (R_RDPBufferPtr)
        addi    R_InputCommandsPtr, 4
        addi    R_RDPBufferPtr, 4
        addi    $t1, -1
        bgtz    $t1, 1b

        jr          $ra
        nop

############
# DL_DMA commands
# These consist of a command and a pointer to RDRAM.
# The pointer contains the data that the command uses.
# These are for, say, loading something into DMEM.

DL_DMA_LoadProjectionMatrix:
# Command layout:
# $40 | 24 bits of pointer                                
# 32 bits of null
        li      $t1, MatrixProjectionInt
        li      $t2, MatrixProjectionFrac
        b       ReadMatrixDMA

###
DL_DMA_LoadModelMatrix:
# Command layout:
# $43 | 24 bits of pointer                                
# 32 bits of null
        li      $t1, MatrixModelInt
        li      $t2, MatrixModelFrac
        b       ReadMatrixDMA

###
DL_DMA_LoadViewMatrix:
# Command layout:
# $44 | 24 bits of pointer                                
# 32 bits of null
        li      $t1, MatrixViewInt
        li      $t2, MatrixViewFrac
        b       ReadMatrixDMA

###
DL_DMA_LoadTransformationMatrix:
# Command layout:
# $46 | 24 bits of pointer                                
# 32 bits of null
        li      $t1, MatrixTransformInt
        li      $t2, MatrixTransformFrac
        b       ReadMatrixDMA

###
ReadMatrixDMA:
        lw          $t3, (R_InputCommandsPtr)          # fetch 32 bits from the buffer into $t0
        li          $t4, 0x00FFFFFF
        and         $t3, $t3, $t4       # $t3 = $t3 & $t4, masking off the command byte
        
        li          $t4, TempDMA
        mtc0        $t4, $0             # DMEM address to receive DMA data
        mtc0        $t3, $1             # RDRAM source
        li          $t3, 64
        mtc0        $t3, $2             # read 64 bytes into TempDMA

1:
        mfc0        $t7, $6             # Read DMA Busy register
        bgtz        $t7, 1b             # Loop until DMA is available.

        li          $t0, TempDMA
        li          $t4, 16             # 16 numbers to process
        li          $t5, 0

2:
        lw          $t3, ($t0)          # read a 16.16 number
        sh          $t3, ($t2)          # store the fraction       
        sra         $t3, $t3, 16        # get the int
        sh          $t3, ($t1)          # store the int

        addi        $t0, 4              # advance TempDMA
        addi        $t1, 2              # advance int buffer
        addi        $t2, 2              # advance frac buffer

        addi        $t5, 1              # decrement loops remaining        
        bne         $t5, $t4, 2b        # loop if any remaining
        nop
        
        addi        R_InputCommandsPtr, 8 # advance 2 words                        
        
        jr          $ra                 # done
        nop                             # delay slot
        
DL_DMA_LoadVertexBuffer:
#	HI: $41AAAAAA
#	LO: $SSCC0000
#
#	A = rdram_ptr
#	S = start
#	C = count

# TODO: This isn't calculating the correct transfer size.

#define R_rdram_ptr    $a0
#define R_vertex_start $a1
#define R_vertex_count $a2

        lw          R_rdram_ptr, (R_InputCommandsPtr) # fetch 32 bits from the buffer
        addi        R_InputCommandsPtr, 4 # advance to the low word of the command

        lw          $t0, (R_InputCommandsPtr) # temp storage for the low word

        # The low byte of R_vertex_start is now S from the low word.                                
        srl         R_vertex_start, $t0, 24

        # The low byte of R_vertex_count is now C from the low word.
        srl         R_vertex_count, $t0, 16
        andi        R_vertex_count, R_vertex_count, 0x00FF       
        
        # Get the vertex data, 64 bytes at a time.
        li          $t0, 0x00FFFFFF     
        and         R_rdram_ptr, R_rdram_ptr, $t0 # mask off the command byte
        
        # OK, now figure out where in the vertex buffer to start.
        li          $t7, VertexTable-16
.vertex_buffer_loop:           
        addi        $t7, 16             # advance one position
        addi        R_vertex_start, -1  # decrement counter
        bgtz        R_vertex_start, .vertex_buffer_loop # loop until we're out of vertices

        # Now tally how many bytes we need to read with DMA.
        sll         $t3, R_vertex_count,4 # R_vertex_count * 16 bytes = DMA read length
        
        mtc0        $t7, $0             # DMA dest: VertexTable + (16*start)
        mtc0        R_rdram_ptr, $1     # DMA source: R_rdram_ptr
        mtc0        $t3, $2             # DMA read length: R_vertex_count * 16 bytes
        
.cmd_done:       
        addi        R_InputCommandsPtr, 4 # advance command pointer to next command
        jr          $ra
        nop
        
#undef R_rdram_ptr
#undef R_vertex_start
#undef R_vertex_count

DL_DrawTriangle:
        # Do a little initialization.
        #define VECTOR_ZERO VEC31
        
        # Set up VEC31 as all 0s.
        sw          $0, 0xA00
        sw          $0, 0xA04
        sw          $0, 0xA08
        sw          $0, 0xA0C
        li          $a3, 0xA00
        lqv         VECTOR_ZERO, 0, 0, S_A3 # clear VEC31's first quad

        # Set up VEC30 as a constant, 2.
        li          $t0,0x00020002
        sw          $t0,Test0
        li          $t0,Test0
        llv         VEC30, 0, 0, S_T0
        llv         VEC30, 4, 0, S_T0
        llv         VEC30, 8, 0, S_T0
        llv         VEC30, 12, 0, S_T0

        # Load the triangle code.
        PUSH    $ra
        lw      $a0, 0x804      # get the address of the triangle code segment
        jal     LoadOverlaySegment
        jal     0x400
        POP     $ra

        jr      $ra
        nop

LoadOverlaySegment:
        # Is the requested segment already paged in?
        lw      $t0, LoadedSegmentAddress
        beq     $t0, $a0, 9f
        nop

        # Load segment "DRAWTRI" into IMEM+0x400 and jump to 0x400.
        li      $t0, 0x1400
        mtc0    $t0, $0         # DMA destination: 0x400 in IMEM
     
        mtc0    $a0, $1         # DMA source: (value of 0x804 in DMEM)
        sw      $a0, LoadedSegmentAddress

        li      $t0, 3072
        mtc0    $t0, $2         # DMA length: 3072 bytes

        # Block until DMA is complete.
1:
        mfc0    $t0, $6
        bgtz    $t0, 1b # Loop until RSP register 6 is zero
        nop

9:
        jr      $ra
        nop

#
# EraseDisplayList:
#         PUSH    $t0
#         PUSH    $t1

#         li      $t0, 0x3FC
#         li      $t1, 4
        
# _EraseLoop:
#         sw      $zero, ($t0)
#         sub     $t0, $t0, $t1
#         bgtz    $t0, _EraseLoop

#         POP     $t1
#         POP     $t0

#         jr      $ra
#         nop
# #

#######################################
# Display list command jump table
DL_AdvanceTwoWords:
InvalidCommand:
        addi        R_InputCommandsPtr, 8
        addi        R_RDPBufferPtr, 8
        
        jr          $ra
        nop

        .org 0x400

        .org 0xE00
DisplayListCommandJumpTable:
        .word       DL_NOP              # $00
        .word       InvalidCommand      # $01
        .word       InvalidCommand      # $02
        .word       InvalidCommand      # $03
        .word       InvalidCommand      # $04
        .word       InvalidCommand      # $05
        .word       InvalidCommand      # $06
        .word       InvalidCommand      # $07
        .word       InvalidCommand      # $08
        .word       InvalidCommand      # $09
        .word       InvalidCommand      # $0A
        .word       InvalidCommand      # $0B
        .word       InvalidCommand      # $0C
        .word       InvalidCommand      # $0D
        .word       InvalidCommand      # $0E
        .word       InvalidCommand      # $0F
        .word       InvalidCommand      # $10
        .word       InvalidCommand      # $11
        .word       InvalidCommand      # $12
        .word       InvalidCommand      # $13
        .word       InvalidCommand      # $14
        .word       InvalidCommand      # $15
        .word       InvalidCommand      # $16
        .word       InvalidCommand      # $17
        .word       InvalidCommand      # $18
        .word       InvalidCommand      # $19
        .word       InvalidCommand      # $1A
        .word       InvalidCommand      # $1B
        .word       InvalidCommand      # $1C
        .word       InvalidCommand      # $1D
        .word       InvalidCommand      # $1E
        .word       InvalidCommand      # $1F
        .word       InvalidCommand      # $20
        .word       InvalidCommand      # $21
        .word       InvalidCommand      # $22
        .word       InvalidCommand      # $23
        .word       DL_TextureRectangle # $24
        .word       InvalidCommand      # $25
        .word       InvalidCommand      # $26
        .word       DL_SyncPipe         # $27
        .word       DL_SyncTile         # $28
        .word       DL_SyncFull         # $29
        .word       InvalidCommand      # $2A
        .word       InvalidCommand      # $2B
        .word       InvalidCommand      # $2C
        .word       DL_SetScissor       # $2D
        .word       DL_SetPrimDepth     # $2E
        .word       DL_SetOtherModes    # $2F
        .word       DL_LoadTlut         # $30
        .word       DL_SyncLoad         # $31
        .word       DL_SetTileSize      # $32
        .word       DL_LoadBlock        # $33
        .word       DL_LoadTile         # $34
        .word       DL_SetTile          # $35
        .word       DL_FillRectangle    # $36
        .word       DL_SetFillColor     # $37
        .word       DL_SetFogColor      # $38
        .word       DL_SetBlendColor    # $39
        .word       DL_SetPrimColor     # $3A
        .word       DL_SetEnvColor      # $3B
        .word       DL_SetCombineMode   # $3C
        .word       DL_SetTextureImage  # $3D
        .word       DL_SetZImage        # $3E
        .word       DL_SetColorImage    # $3F

        # Software commands
        .word   DL_DMA_LoadProjectionMatrix     # $40
        .word   DL_DMA_LoadVertexBuffer         # $41
        .word   DL_DrawTriangle                 # $42
        .word   DL_DMA_LoadModelMatrix          # $43
        .word   DL_DMA_LoadViewMatrix           # $44
        .word   InvalidCommand                  # $45
        .word   DL_DMA_LoadTransformationMatrix # $46

        .org 0x1000,0x00
        

##############################
        .section .data
        # 0x000-0x7FF - display list
        # 0x800-0x9FF - shared variables
        # 0xA00-0xCFF - local variables
        # 0xD00-0xDFF - vertex table
        # 0xE00-0xFFF - command jump table

        .org 0x010
        # RDP command ring buffer - for sending commands to the RDP.
RDPCommandRingBuffer: .space 1008
        
        # The raw display list from the CPU. Gets turned into RDP commands.
InputDisplayList:   .space 1024
        
        .org 0x800
        #include "shared_data.S"

        .org 0xA00
        # Unique data for each program
        
        .org 0xD00
        #include "vertex_table.S"
        
        # Space for the command jump table
        .org 0xE00
DisplayListJumpTable: .space 512