#include "hello.hpp"

#include "assets/textures/textures.h"
#include "shooter/core.h"
#include "audioint.h"
#include "kategl/polyobj/polyobj.hpp"
#include "shooter/polyobj/kinds/cube.hpp"
#include "shooter/polyobj/kinds/cone.hpp"

#include "fonttable.hpp"

Shooter shooter;

char _64Drive_buffer[512];

char sound_buffer[128000] __attribute__((aligned(64)));
uint16_t z_buffer[320*240] __attribute__((aligned(64)));

volatile int ticks;
volatile bool vbl_flag;
volatile int rdp_ticks_waiting;
volatile int rsp_ticks_waiting;

Cube *objCube;
Cone *objCone;

bool enable_rotation = true;

KateGL::FX_Matrix44 transformationStack[16];
int transformationStackPosition = 0;

Pak dataPak;

KateGL::FX_Matrix44 * CurrentTransformationMatrix()
{
	return (KateGL::FX_Matrix44 *)(UncachedAddr(&(transformationStack[transformationStackPosition])));
}

void PushMatrix()
{
	data_cache_hit_writeback_invalidate(&(transformationStack[transformationStackPosition]), sizeof(KateGL::FX_Matrix44));

	for(int i=0; i<4; i++)
	{
		for(int j=0; j<4; j++)
		{
			transformationStack[transformationStackPosition+1].data[i][j] = transformationStack[transformationStackPosition].data[i][j];
		}
	}

	// sprintf(dbg, "*** pushmatrix pre: stack ptr is %p ***\n", &(transformationStack[transformationStackPosition]));
	// _64Drive_putstring(dbg);
	// sprintf(dbg, "%08X %08X %08X %08X\n", CurrentTransformationMatrix()->data[0][0], CurrentTransformationMatrix()->data[0][1], CurrentTransformationMatrix()->data[0][2], CurrentTransformationMatrix()->data[0][3]);
	// _64Drive_putstring(dbg);
	// sprintf(dbg, "%08X %08X %08X %08X\n", CurrentTransformationMatrix()->data[1][0], CurrentTransformationMatrix()->data[1][1], CurrentTransformationMatrix()->data[1][2], CurrentTransformationMatrix()->data[1][3]);
	// _64Drive_putstring(dbg);
	// sprintf(dbg, "%08X %08X %08X %08X\n", CurrentTransformationMatrix()->data[2][0], CurrentTransformationMatrix()->data[2][1], CurrentTransformationMatrix()->data[2][2], CurrentTransformationMatrix()->data[2][3]);
	// _64Drive_putstring(dbg);
	// sprintf(dbg, "%08X %08X %08X %08X\n", CurrentTransformationMatrix()->data[3][0], CurrentTransformationMatrix()->data[3][1], CurrentTransformationMatrix()->data[3][2], CurrentTransformationMatrix()->data[3][3]);
	// _64Drive_putstring(dbg);

	transformationStackPosition++;
	data_cache_hit_writeback_invalidate(&(transformationStack[transformationStackPosition]), sizeof(KateGL::FX_Matrix44));

	// sprintf(dbg, "*** pushmatrix post: stack ptr is %p ***\n", &(transformationStack[transformationStackPosition]));
	// _64Drive_putstring(dbg);
	// sprintf(dbg, "%08X %08X %08X %08X\n", CurrentTransformationMatrix()->data[0][0], CurrentTransformationMatrix()->data[0][1], CurrentTransformationMatrix()->data[0][2], CurrentTransformationMatrix()->data[0][3]);
	// _64Drive_putstring(dbg);
	// sprintf(dbg, "%08X %08X %08X %08X\n", CurrentTransformationMatrix()->data[1][0], CurrentTransformationMatrix()->data[1][1], CurrentTransformationMatrix()->data[1][2], CurrentTransformationMatrix()->data[1][3]);
	// _64Drive_putstring(dbg);
	// sprintf(dbg, "%08X %08X %08X %08X\n", CurrentTransformationMatrix()->data[2][0], CurrentTransformationMatrix()->data[2][1], CurrentTransformationMatrix()->data[2][2], CurrentTransformationMatrix()->data[2][3]);
	// _64Drive_putstring(dbg);
	// sprintf(dbg, "%08X %08X %08X %08X\n", CurrentTransformationMatrix()->data[3][0], CurrentTransformationMatrix()->data[3][1], CurrentTransformationMatrix()->data[3][2], CurrentTransformationMatrix()->data[3][3]);
	// _64Drive_putstring(dbg);

}

void PopMatrix()
{
	transformationStackPosition--;
}

void dumpRCPInfo()
{
	sprintf(_64Drive_buffer, "*** RCP Status ***\n");
	_64Drive_putstring(_64Drive_buffer);

	sprintf(_64Drive_buffer, "PC: %04X | RSP STATUS: %08X\n", MMIO32(0xA4080000), MMIO32(0xA4040010));
	_64Drive_putstring(_64Drive_buffer);

	sprintf(_64Drive_buffer, "RDP CMD_START: %04X | RDP CMD_END: %04X | RDP CMD_CURRENT: %04X\n", MMIO32(0xA4100000), MMIO32(0xA4100004), MMIO32(0xA4100008));
	_64Drive_putstring(_64Drive_buffer);

	sprintf(_64Drive_buffer, "RDP STATUS: %08X\n", MMIO32(0xA410000C));
	_64Drive_putstring(_64Drive_buffer);

	sprintf(_64Drive_buffer, "*** display list:\n");
	_64Drive_putstring(_64Drive_buffer);

	_64Drive_putstring("      00000000 00000004 00000008 0000000C 00000010 00000014 00000018 0000001C 00000020 00000024 00000028 0000002C 00000030 00000034 00000038 0000003C\n");
	int word = 0;
	for(int i=0; i<16; i++)
	{
		sprintf(_64Drive_buffer, "%04X: ", (i*64));
		_64Drive_putstring(_64Drive_buffer);

		for(int j=0; j<16; j++)
		{
			sprintf(_64Drive_buffer, "%08X ", MMIO32((uint32_t)0xA4000000 + (word*4)));
			_64Drive_putstring(_64Drive_buffer);
			word++;
		}
		_64Drive_putstring("\n");
	}

	_64Drive_putstring("\n");
}

void vblCallback(void)
{
    ticks++;
	vbl_flag = true;

	if(rsp_active)
	{
		rsp_ticks_waiting--;

		if(rsp_ticks_waiting > 0)
		{
			return;
		}

		sprintf(_64Drive_buffer, "RSP not responding\n");
		_64Drive_putstring(_64Drive_buffer);

		dumpRCPInfo();		

		while(true) {}
	}
	else
	{
		rsp_ticks_waiting = 60;
	}

	if(waiting_for_rdp)
	{
		rdp_ticks_waiting--;

		if(rdp_ticks_waiting > 0)
		{
			return;
		}

		sprintf(_64Drive_buffer, "RDP not responding\n");
		_64Drive_putstring(_64Drive_buffer);

		dumpRCPInfo();		

		while(true) {}
	}
	else
	{
		rdp_ticks_waiting = 60;
	}
}

void LoadTextures()
{
	// C64 Kernal font
	KateGL::textureManager.AppendToTextureList(
		"F_KRN_UP", kernal_upper,
		128, 16,
		KL_TEX_FORMAT_I, KL_TEX_DEPTH_8BIT,
		kernal_upper_tlut, 16);
	KateGL::textureManager.AppendToTextureList(
		"F_KRN_LO", kernal_lower,
		128, 16,
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT,
		kernal_lower_tlut, 16);
	KateGL::textureManager.AppendToTextureList(
		"F_KRN_NO", kernal_numbers,
		128, 16,
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT,
		kernal_numbers_tlut, 16);
	KateGL::textureManager.AppendToTextureList(
		"F_KERNAL", kernal_numbers,
		128, 16,
		KL_TEX_FORMAT_I, KL_TEX_DEPTH_4BIT,
		NULL, 0);

	KateGL::textureManager.AppendToTextureList(
		"PROHIB", prohibitory,
		32, 32,
		KL_TEX_FORMAT_RGBA, KL_TEX_DEPTH_16BIT,
		NULL, 0);

	KateGL::textureManager.AppendToTextureList(
		TEX_F_ARIALCAPS, arialcaps,
		212, 9,
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT,
		arialcaps_tlut, 17);

	KateGL::textureManager.AppendToTextureList(
		TEX_F_ARIAL1, ariallower,
		64, 12,
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT,
		ariallower_tlut, 172);
	KateGL::textureManager.AppendToTextureList(
		TEX_F_ARIAL2, ariallower+(64*12),
		64, 11,
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT,
		ariallower_tlut, 172);
	KateGL::textureManager.AppendToTextureList(
		TEX_F_ARIAL3, ariallower2,
		64, 12,
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT,
		ariallower2_tlut, 180);
	KateGL::textureManager.AppendToTextureList(
		TEX_F_ARIAL4, ariallower2+(64*12),
		64, 12,
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT,
		ariallower2_tlut, 180);

	// USP Talon movement frames
	KateGL::textureManager.AppendToTextureList(
		TEX_TALONLL, usptalon_left2, 
		32, 25, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		usptalon_left2_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_TALONL,	usptalon_left1, 
		32, 25, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		usptalon_left1_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_TALON, usptalon_middle, 
		32, 25,
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		usptalon_middle_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_TALONR, usptalon_right1,
		32, 25, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		usptalon_right1_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_TALONRR, usptalon_right2, 
		32, 25, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		usptalon_right2_tlut, 32);

	// U-Ship movement frames
	KateGL::textureManager.AppendToTextureList(
		TEX_USHIP1, uship1, 
		16, 19, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		uship1_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_USHIP2, uship2, 
		16, 19, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		uship2_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_USHIP3, uship3, 
		16, 19, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		uship3_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_USHIP4, uship4, 
		16, 19, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		uship4_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_USHIP5, uship5, 
		16, 19, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		uship5_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_USHIP6, uship6, 
		16, 19, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		uship6_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_USHIP7, uship7, 
		16, 19, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		uship7_tlut, 32);
	KateGL::textureManager.AppendToTextureList(
		TEX_USHIP8, uship8, 
		16, 19, 
		KL_TEX_FORMAT_CI, KL_TEX_DEPTH_8BIT, 
		uship8_tlut, 32);

	// Bullets
	KateGL::textureManager.AppendToTextureList(
		TEX_BULLETS, tyrianbullet, 
		8, 12, 
		KL_TEX_FORMAT_RGBA, KL_TEX_DEPTH_16BIT,
		NULL, 0);

	sprintf(_64Drive_buffer, "Texture list contains %d textures\n", KateGL::textureManager.TextureCount());
	_64Drive_putstring(_64Drive_buffer);
}

void print_debug_info()
{
	Fixed vertex1_x, vertex1_y, vertex1_z, vertex1_w, vertex2_x, vertex2_y, vertex2_z, vertex2_w, vertex3_x, vertex3_y, vertex3_z, vertex3_w;
	vertex1_x = MMIO32((uint32_t)0xA4000960); vertex1_y = MMIO32((uint32_t)0xA4000964); vertex1_z = MMIO32((uint32_t)0xA4000968); vertex1_w = MMIO32((uint32_t)0xA400096C); 
	vertex2_x = MMIO32((uint32_t)0xA4000970); vertex2_y = MMIO32((uint32_t)0xA4000974); vertex2_z = MMIO32((uint32_t)0xA4000978); vertex2_w = MMIO32((uint32_t)0xA400097C);
	vertex3_x = MMIO32((uint32_t)0xA4000980); vertex3_y = MMIO32((uint32_t)0xA4000984); vertex3_z = MMIO32((uint32_t)0xA4000988); vertex3_w = MMIO32((uint32_t)0xA400098C);

	sprintf(_64Drive_buffer, "v1 %08lX %08lX %08lX %08lX\n", vertex1_x, vertex1_y, vertex1_z, vertex1_w);
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "v2 %08lX %08lX %08lX %08lX\n", vertex2_x, vertex2_y, vertex2_z, vertex2_w);
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "v3 %08lX %08lX %08lX %08lX\n", vertex3_x, vertex3_y, vertex3_z, vertex3_w);
	_64Drive_putstring(_64Drive_buffer);

	Fixed dxhdy = ( vertex3_y == vertex1_y ) ? 0 : ( FX_Divide((vertex3_x - vertex1_x), (vertex3_y - vertex1_y)) );
	Fixed dxmdy = ( vertex2_y == vertex1_y ) ? 0 : ( FX_Divide((vertex2_x - vertex1_x), (vertex2_y - vertex1_y)) );
	Fixed dxldy = ( vertex3_y == vertex2_y ) ? 0 : ( FX_Divide((vertex3_x - vertex2_x), (vertex3_y - vertex2_y)) );

	// _____
	// Xh-Yl is the major edge.
	// _____
	// Xm-Yl is the middle edge. 
	// _____
	// Xl-Yl is the low edge.

	int winding = ( vertex1_x * vertex2_y - vertex2_x * vertex1_y ) + ( vertex2_x * vertex3_y - vertex3_x * vertex2_y ) + ( vertex3_x * vertex1_y - vertex1_x * vertex3_y );
	int flip = ( winding > 0 ? 1 : 0 );
	sprintf(_64Drive_buffer, "Expected flip bit: %d\n", flip);
	_64Drive_putstring(_64Drive_buffer);

	Fixed _Xh = FX_Add(vertex1_x, FX_Multiply((vertex1_y & 0xFF000000) - vertex1_y, dxhdy));
	Fixed _Xm = FX_Add(vertex1_x, FX_Multiply((vertex1_y & 0xFF000000) - vertex1_y, dxmdy));

	sprintf(_64Drive_buffer, "Xh: X0 + (scanlineY - Y0) * DXhDY = %08lX (expected %08lX)\n", MMIO32((uint32_t)0xA40009A8), _Xh);
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "Xm: X0 + (scanlineY - Y0) * DXmDY = %08lX (expected %08lX)\n", MMIO32((uint32_t)0xA40009Ac), _Xm);
	_64Drive_putstring(_64Drive_buffer);

	sprintf(_64Drive_buffer, "Xh %08lX Yh %08lX\n", MMIO32((uint32_t)0xA40009A8), MMIO32((uint32_t)0xA400099C));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "Xm %08lX Ym %08lX\n", MMIO32((uint32_t)0xA40009AC), MMIO32((uint32_t)0xA40009A0));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "Xl %08lX Yl %08lX\n", MMIO32((uint32_t)0xA40009B0), MMIO32((uint32_t)0xA40009A4));
	_64Drive_putstring(_64Drive_buffer);

	// The problem is that we're always using vertices in Y ascending order for calculating X coefficients which isn't necessarily true!
	Fixed leftmost_x = vertex2_x; // debug

	sprintf(_64Drive_buffer, "dxhdy: %08lX (expected: %08lX)\n", MMIO32((uint32_t)0xA40009B4), dxhdy);
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "dxmdy: %08lX (expected: %08lX)\n", MMIO32((uint32_t)0xA40009B8), dxmdy);
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "dxldy: %08lX (expected: %08lX)\n", MMIO32((uint32_t)0xA40009BC), dxldy);
	_64Drive_putstring(_64Drive_buffer);

	sprintf(_64Drive_buffer, "Inverse Depth: %08lX\n", MMIO32((uint32_t)0xA4000A00));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "ZDzDx: %08lX\n", MMIO32((uint32_t)0xA4000A04));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "ZDzDe: %08lX\n", MMIO32((uint32_t)0xA4000A08));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "ZDzDy: %08lX\n", MMIO32((uint32_t)0xA4000A0C));
	_64Drive_putstring(_64Drive_buffer);

	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000A0));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000A4));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000A8));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000AC));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000B0));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000B4));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000B8));
	_64Drive_putstring(_64Drive_buffer);
	sprintf(_64Drive_buffer, "%08lX\n", MMIO32((uint32_t)0xA40000BC));
	_64Drive_putstring(_64Drive_buffer);
}

float scale_factor_x = 1.0;
float scale_factor_y = 1.0;

int vert = 100, vert2 = 40;
int horiz = 40, horiz2 = 200;
int add_to_vert = 1, add_to_vert2 = 1;
int add_to_horiz = 1, add_to_horiz2 = -1;

void DL_3DCube(display_context_t context)
{
	// Build a display list in the context given. Transfer it to the RDP once built.
	display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

	KateGL::DL::klPreamble(&display_list_current);

	rdp_attach_display(&display_list_current, context);
	rdp_set_combine_mode(&display_list_current, CC_C0_RGB_MUL_ZERO_COLOR|CC_C1_RGB_MUL_ZERO_COLOR|CC_C0_RGB_ADD_SHADE_COLOR|CC_C1_RGB_ADD_SHADE_COLOR);
	rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_1CYCLE|MODE_IMAGE_READ_EN|MODE_CVG_DEST_CLAMP|MODE_ALPHA_CVG_SELECT);
	rdp_set_z_image(&display_list_current, (uint16_t *)z_buffer);
	rdp_sync(&display_list_current, SYNC_PIPE);

	BASIC_LoadTransformationMatrix(&display_list_current, (uint32_t *)CurrentTransformationMatrix());

	int trianglesRemaining = objCube->GetMesh()->triangleCount;
	for(int i=0; i<objCube->GetMesh()->vertexCount/15 + 1; i++)
	{
		BASIC_LoadVertexBuffer(&display_list_current, &(objCube->GetMesh()->vertices[15*i]), 0, 15);

		for(int v=0; v<5; v++)
		{
			BASIC_DrawTriangle(&display_list_current, (3*v), (3*v)+1, (3*v)+2);
			if(--trianglesRemaining == 0) break;
		}
	}

	rdp_sync(&display_list_current, SYNC_FULL);

	rdp_end_display_list(&display_list_current);
}

void DL_3DCone(display_context_t context)
{
	// Build a display list in the context given. Transfer it to the RDP once built.
	display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

	KateGL::DL::klPreamble(&display_list_current);

	rdp_attach_display(&display_list_current, context);
	rdp_set_combine_mode(&display_list_current, CC_C0_RGB_MUL_ZERO_COLOR|CC_C1_RGB_MUL_ZERO_COLOR|CC_C0_RGB_ADD_SHADE_COLOR|CC_C1_RGB_ADD_SHADE_COLOR);
	rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_1CYCLE|MODE_IMAGE_READ_EN|MODE_CVG_DEST_CLAMP|MODE_ALPHA_CVG_SELECT);
	rdp_set_z_image(&display_list_current, (uint16_t *)z_buffer);
	rdp_sync(&display_list_current, SYNC_PIPE);

	BASIC_LoadTransformationMatrix(&display_list_current, (uint32_t *)CurrentTransformationMatrix());

	auto meshCone = objCone->GetMesh();

	int trianglesRemaining = objCone->GetMesh()->triangleCount - 3;
	// for(int i=0; i<(meshCone->vertexCount/15)+1; i++)
	// for(int i=0; i<3; i++)
	// {
	// 	BASIC_LoadVertexBuffer(&display_list_current, &(objCone->GetMesh()->vertices[15*i]), 0, 15);

	// 	for(int v=0; v<5; v++)
	// 	{
	// 		BASIC_DrawTriangle(&display_list_current, (3*v), (3*v)+1, (3*v)+2);
	// 		if(--trianglesRemaining == 0) break;
	// 	}
	// }

	BASIC_LoadVertexBuffer(&display_list_current, &(objCone->GetMesh()->vertices[0]), 0, 15);
	BASIC_DrawTriangle(&display_list_current, 0, 1, 2);
	BASIC_DrawTriangle(&display_list_current, 3, 4, 5);
	BASIC_DrawTriangle(&display_list_current, 6, 7, 8);
	BASIC_DrawTriangle(&display_list_current, 9, 10, 11);
	BASIC_DrawTriangle(&display_list_current, 12, 13, 14);

	// rdp_sync(&display_list_current, SYNC_PIPE);
	BASIC_LoadVertexBuffer(&display_list_current, &(objCone->GetMesh()->vertices[15]), 0, 15);
	BASIC_DrawTriangle(&display_list_current, 0, 1, 2);
	BASIC_DrawTriangle(&display_list_current, 3, 4, 5);
	BASIC_DrawTriangle(&display_list_current, 6, 7, 8);
	BASIC_DrawTriangle(&display_list_current, 9, 10, 11);
	// BASIC_DrawTriangle(&display_list_current, 12, 13, 14);

	BASIC_LoadVertexBuffer(&display_list_current, &(objCone->GetMesh()->vertices[30]), 0, 12);
	BASIC_DrawTriangle(&display_list_current, 0, 1, 2);
	BASIC_DrawTriangle(&display_list_current, 3, 4, 5);
	BASIC_DrawTriangle(&display_list_current, 6, 7, 8);
	BASIC_DrawTriangle(&display_list_current, 9, 10, 11);

	rdp_sync(&display_list_current, SYNC_FULL);

	rdp_end_display_list(&display_list_current);
}

void wait_frames(int num)
{
	int target_ticks = ticks + num;

	while(ticks < target_ticks)
	{
		//sprintf(_64Drive_buffer, "Waiting for frame %d. Current frame is %d\n", target_ticks, ticks);
		//_64Drive_putstring(_64Drive_buffer);	
	}
}

void clear_rsp_buffers()
{
	for(int i=0; i<512; i++)
	{
		MMIO32(SP_DMEM_BASE + (4*i)) = 0;
	}
}

void ConstructModelMatrix(KateGL::PolyObject *obj)
{
	KateGL::FX_Matrix44 model_rotation_x, model_rotation_y, model_rotation_z;

	data_cache_hit_writeback_invalidate(&model_matrix, sizeof(KateGL::FX_Matrix44));
	KateGL::klUnitMatrix(&model_matrix);

	KateGL::klTranslation((&model_translation), obj->GetPosition().x, obj->GetPosition().y, obj->GetPosition().z);
	data_cache_hit_writeback_invalidate(&model_translation, sizeof(KateGL::FX_Matrix44));
	KateGL::klMatrixMultiply((&model_translation), (&model_matrix), (&model_matrix));

	KateGL::klRotationDegreesX((&model_rotation_x), int(obj->GetRotation().x));
	KateGL::klRotationDegreesY((&model_rotation_y), int(obj->GetRotation().y));
	KateGL::klRotationDegreesZ((&model_rotation_z), int(obj->GetRotation().z));
	KateGL::klMatrixMultiply((&model_rotation_x), (&model_matrix), (&model_matrix));
	KateGL::klMatrixMultiply((&model_rotation_y), (&model_matrix), (&model_matrix));
	KateGL::klMatrixMultiply((&model_rotation_z), (&model_matrix), (&model_matrix));

	KateGL::klScale((&model_scale), FX_FromFloat(obj->GetScale().x), FX_FromFloat(obj->GetScale().y), FX_FromFloat(obj->GetScale().z));
	data_cache_hit_writeback_invalidate(&model_matrix, sizeof(KateGL::FX_Matrix44));
	
	KateGL::klMatrixMultiply((&model_scale), (&model_matrix), (&model_matrix));
}

void DL_PutStringArial12(display_context_t context, char *str, Point2D origin);
void DL_WriteString4BPP(display_context_t context, char *str, char *fontname, int x, int y, float scaleX, float scaleY);

int main()
{
	// Set up the RSP segments.
	RSPSegmentManager::InitSegmentsList();
	RSPSegmentManager::CopySegmentAddressesToDMEM();
	RSPSegmentManager::LoadBootSegment();

	rsp_active = false;
	waiting_for_rdp = false;

	// Wait for the 64drive to become available.
	// If it never does, assume we don't have one hooked up.
	_64Drive_wait();
	ticks = 0;

	sprintf(_64Drive_buffer, "\n\n*** N64 Boot ***\n");
	_64Drive_putstring(_64Drive_buffer);

	_64Drive_putstring("Initializing font atlases\n");
	setup_arial_font();

	_64Drive_putstring("Initializing MVP matrices\n");

	// Start building matrices that we need.
	klPerspective(&perspective_matrix, 1, 500);
	klUnitMatrix(&view_matrix);
	klUnitMatrix(&model_matrix);

	// Make sure we aren't caching these values.
	data_cache_hit_writeback_invalidate(&perspective_matrix, sizeof(KateGL::FX_Matrix44));
	data_cache_hit_writeback_invalidate(&view_matrix, sizeof(KateGL::FX_Matrix44));
	data_cache_hit_writeback_invalidate(&model_matrix, sizeof(KateGL::FX_Matrix44));

	_64Drive_putstring("Initializing interrupts.\n");

	init_interrupts();
	register_VI_handler(vblCallback); // VBlank handler
	_64Drive_putstring("VBL handler installed\n");

	// Get a context and blank the screen.
	_64Drive_putstring("Initializing display: 320x240, 16bpp.\n");
	display_init(RESOLUTION_320x240, DEPTH_16_BPP, 2, GAMMA_NONE, ANTIALIAS_RESAMPLE);
	set_VI_interrupt(1, 400);

	_64Drive_putstring("Initializing RDP.\n");
	rdp_init();
	_64Drive_putstring("Initializing RSP.\n");
	rsp_init();
	_64Drive_putstring("Initializing timers.\n");
	timer_init();
	_64Drive_putstring("Initializing controllers.\n");
	controller_init();
	_64Drive_putstring("Initializing DFS.\n");
	if(dfs_init(DFS_DEFAULT_LOCATION) != DFS_ESUCCESS)
	{
		_64Drive_putstring("Failed to initialize DFS!\n");
		while(true) {};
	}

	dataPak.LoadFromDFS("test.pak");
	// auto ptr = dataPak.GetFileDataPtr("testfile.txt");
	// sprintf(_64Drive_buffer, "file data is at %p\n", ptr);
	// _64Drive_putstring(_64Drive_buffer);
	// char buf[128];
	// memcpy(buf, ptr, 40);
	// buf[40] = '\0';
	// sprintf(_64Drive_buffer, "%s\n", buf);
	// _64Drive_putstring(_64Drive_buffer);

	// memset(sound_buffer, 0, 128000);

	// int soundHandle = dfs_open("sine.wav");
	// int soundSize = dfs_size(soundHandle);
	// sprintf(_64Drive_buffer, "sound handle: %d, file size is %d bytes\n", soundHandle, soundSize);
	// _64Drive_putstring(_64Drive_buffer);
	// char *soundBuf = (char *)malloc(88200);
	// dfs_seek(soundHandle, 44, SEEK_CUR);
	// dfs_read(soundBuf, 88200, 1, soundHandle);
	// memcpy(aligned_sound_buffer, soundBuf, 88200);

	// MMIO32(UNCACHED_ADDR(AI_DACRATE_REG)) = (VI_NTSC_CLOCK/44100) - 1;
	// MMIO32(UNCACHED_ADDR(AI_BITRATE_REG)) = AI_SAMPLE_16BIT;
	// MMIO32(UNCACHED_ADDR(AI_DRAM_ADDR_REG)) = (uint32_t)aligned_sound_buffer;
	// MMIO32(UNCACHED_ADDR(AI_LEN_REG)) = 88200;
	// MMIO32(UNCACHED_ADDR(AI_CONTROL_REG)) |= AI_CONTROL_DMA_ON;

	LoadTextures();

	wait_frames(60);

	// Acquire the framebuffer.
	_64Drive_putstring("Acquiring a framebuffer and clearing it.\n");

	_64Drive_putstring("Initializing object manager.\n");
	mobjectManager.Initialize();

	int degrees = 0;
	float pos_x = 0, pos_y = 0, pos_z = -10;

	// Copy the command jump table from IMEM to DMEM.
	for(int i=0; i<128; i++)
	{
		MMIO32(BASIC_JumpTableDMEM + (i*4)) = MMIO32(BASIC_JumpTableIMEM + (i*4));
	}

	shooter.Setup();

	/* frame loop */
	_64Drive_putstring("Beginning frame loop\n");
	
	objCube = new Cube();
	objCone = new Cone();

	while(true)
	{
		KateGL::FX_Matrix44 view_rotation_x, view_rotation_y, view_rotation_z, view_translation;

		// Start of a new frame.
		vbl_flag = false;

		// TODO: Why do we not get a valid context here? Don't we unlock it when display_show occurs?
		display_context_t disp_context = display_lock();
		if(disp_context == 0)
		{
			_64Drive_putstring("got an invalid disp_context");
		}

		// Reset the model/view/transformation matrixes for a new frame.
		KateGL::klUnitMatrix(&model_matrix);
		KateGL::klUnitMatrix(&view_matrix);
		KateGL::klUnitMatrix(CurrentTransformationMatrix());

		// Construct the view transformation.
		KateGL::klTranslation(&view_translation, FX_FromFloat(pos_x), FX_FromInt(0), FX_FromFloat(pos_z));
		KateGL::klMatrixMultiply(&view_translation, &view_matrix, &view_matrix);

		// Flush the caches for our matrices.
		data_cache_hit_writeback_invalidate(&model_matrix, sizeof(KateGL::FX_Matrix44));
		data_cache_hit_writeback_invalidate(&view_translation, sizeof(KateGL::FX_Matrix44));
		data_cache_hit_writeback_invalidate(&view_matrix, sizeof(KateGL::FX_Matrix44));
		data_cache_hit_writeback_invalidate(&perspective_matrix, sizeof(KateGL::FX_Matrix44));

		// Compose the transformation.
		KateGL::klMatrixMultiply(&perspective_matrix, CurrentTransformationMatrix(), CurrentTransformationMatrix());
		KateGL::klMatrixMultiply(&view_matrix, CurrentTransformationMatrix(), CurrentTransformationMatrix());

		// TODO: Find a way to do this that doesn't disable interrupts.
		// Acquire a display context. If 0, no backbuffer is available so wait for one to become available.
		// sprintf(_64Drive_buffer, "Display Context: %d\n", disp_context);
		// _64Drive_putstring(_64Drive_buffer);

		if(enable_rotation) degrees = (degrees + 1) % 360;

		DL_ClearDepthBuffer(disp_context);
		run_ucode();
		wait_for_rsp();
		wait_for_rdp();

		DL_ClearFrameBuffer(disp_context);
		run_ucode();
		wait_for_rsp();
		wait_for_rdp();

		// {
		// 	PushMatrix();

		// 	objCube->SetTranslation(FX_FromInt(-2), 0, 0);
		// 	objCube->SetRotation(-degrees, -degrees, -degrees);
		// 	ConstructModelMatrix(static_cast<KateGL::PolyObject *>(objCube));
		// 	KateGL::klMatrixMultiply(&model_matrix, CurrentTransformationMatrix(), CurrentTransformationMatrix());

		// 	DL_3DCube(disp_context);
		// 	run_ucode();
		// 	wait_for_rsp();
		// 	wait_for_rdp();

		// 	PopMatrix();
		// }
		
		// {
		// 	PushMatrix();

		// 	objCube->SetTranslation(FX_FromInt(2), 0, 0);
		// 	objCube->SetScale(1, 1, 1);
		// 	objCube->SetRotation(degrees, degrees, degrees);
		// 	ConstructModelMatrix(static_cast<KateGL::PolyObject *>(objCube));
		// 	KateGL::klMatrixMultiply(&model_matrix, CurrentTransformationMatrix(), CurrentTransformationMatrix());
			
		// 	DL_3DCube(disp_context);
		// 	run_ucode();
		// 	wait_for_rsp();
		// 	wait_for_rdp();
			
		// 	PopMatrix();
		// }

		// Perform the Shooter subsystem logic.
		shooter.FrameLoop(disp_context);

		// DL_WriteString(disp_context, "KateGL Font Rendering Test", TEX_F_KERNAL, 50, 40, 1, 1);
		// DL_PutStringArial12(disp_context, "i need capital letters now", {.x = 40, .y = 180});

		// Wait for a vblank, then swap buffers.
		while(vbl_flag == false) {};
		display_show(disp_context);

		// dumpRCPInfo();
		// while(true) {};

	}

	while(true) {};
}

void DL_PutStringArial12(display_context_t context, char *str, Point2D origin)
{
	KateGL::TextureListEntry *arial = KateGL::textureManager.GetTextureByName(TEX_F_ARIALCAPS);
	KateGL::TextureListEntry *ariallo1 = KateGL::textureManager.GetTextureByName(TEX_F_ARIAL1);
	KateGL::TextureListEntry *ariallo2 = KateGL::textureManager.GetTextureByName(TEX_F_ARIAL2);
	KateGL::TextureListEntry *ariallo3 = KateGL::textureManager.GetTextureByName(TEX_F_ARIAL3);
	KateGL::TextureListEntry *ariallo4 = KateGL::textureManager.GetTextureByName(TEX_F_ARIAL4);

	display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

	KateGL::DL::klPreamble(&display_list_current);
	KateGL::DL::klUseDisplayContext(&display_list_current, context);

	// rdp_set_combine_mode(&display_list_current,
	// 	CC_C0_RGB_SUBA_PRIM_COLOR|CC_C1_RGB_SUBA_PRIM_COLOR|
	// 	CC_C0_RGB_SUBB_ZERO_COLOR|CC_C1_RGB_SUBB_ZERO_COLOR|
	// 	CC_C0_RGB_MUL_TEXEL0_COLOR|CC_C1_RGB_MUL_TEXEL0_COLOR|
	// 	CC_C0_RGB_ADD_ZERO_COLOR|CC_C1_RGB_ADD_ZERO_COLOR| 
	// 	CC_C0_ALPHA_MUL_ZERO|CC_C0_ALPHA_ADD_PRIM|
	// 	CC_C1_ALPHA_MUL_ZERO|CC_C1_ALPHA_ADD_PRIM);

	// rdp_set_primitive_color(&display_list_current, 0xC0C0C0FF);

	KateGL::DL::klCombineModeTexturedRectangle(&display_list_current);
	rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_COPY|MODE_EN_TLUT|MODE_ALPHA_COMPARE_EN);

	// sprintf(_64Drive_buffer, "letter_texture: size %d %d coord %d %d\n", letter_texture.size.x, letter_texture.size.y, letter_texture.coord.x, letter_texture.coord.y);
	// _64Drive_putstring(_64Drive_buffer);

	int length = strlen(str);

	int currentX = origin.x;
	int currentY = origin.y;

	for(int group=0; group<(length/5)+1; group++)
	{
		display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;
		KateGL::DL::klPreamble(&display_list_current);
		KateGL::DL::klUseDisplayContext(&display_list_current, context);
			
		KateGL::DL::klCombineModeTexturedRectangle(&display_list_current);
		rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_COPY|MODE_EN_TLUT|MODE_ALPHA_COMPARE_EN);

		bool odd = false;

		for(int i=(group*5); i<(group*5)+5; i++)
		{
			if(str[i] == 0x20)
			{
				currentX += 4;
				continue;
			}
			if(str[i] == '\n')
			{
				currentX = origin.x;
				currentY += 12;
				continue;
			}

			KateGL::DL::klPipeSync(&display_list_current);

			if(strcmp(atlas_ariallower.atlastable[str[i]].texture_name, TEX_F_ARIAL1) == 0)
			{
				KateGL::textureManager.CacheTexture(&display_list_current, ariallo1);
			}
			else if(strcmp(atlas_ariallower.atlastable[str[i]].texture_name, TEX_F_ARIAL2) == 0)
			{
				KateGL::textureManager.CacheTexture(&display_list_current, ariallo2);
			}
			else if(strcmp(atlas_ariallower.atlastable[str[i]].texture_name, TEX_F_ARIAL3) == 0)
			{
				KateGL::textureManager.CacheTexture(&display_list_current, ariallo3);
			}
			else if(strcmp(atlas_ariallower.atlastable[str[i]].texture_name, TEX_F_ARIAL4) == 0)
			{
				KateGL::textureManager.CacheTexture(&display_list_current, ariallo4);
			}

			rdp_draw_textured_rectangle_scaled(&display_list_current, TEXSLOT_0, 
				currentX, currentY, 
				currentX+atlas_ariallower.atlastable[str[i]].size.x, currentY+atlas_ariallower.atlastable[str[i]].size.y, 
				0.25, 1, 
				(atlas_ariallower.atlastable[str[i]].coord.x << 5), (atlas_ariallower.atlastable[str[i]].coord.y << 5));	
			currentX += atlas_ariallower.atlastable[str[i]].size.x;
		}

		rdp_sync(&display_list_current, SYNC_FULL);
		rdp_end_display_list(&display_list_current);

		run_ucode();
		wait_for_rsp();
		wait_for_rdp();
	}
}

void DL_WriteString4BPP(display_context_t context, char *str, char *fontname, int x, int y, float scaleX, float scaleY)
{
	KateGL::TextureListEntry *tex = KateGL::textureManager.GetTextureByName(fontname);

	display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;
	KateGL::DL::klPipeSync(&display_list_current);
	KateGL::DL::klPreamble(&display_list_current);
	KateGL::DL::klUseDisplayContext(&display_list_current, context);

	rdp_set_primitive_color(&display_list_current, 0xFFFFFFFF);
	rdp_set_combine_mode(&display_list_current,
		CC_C0_RGB_SUBA_PRIM_COLOR|CC_C1_RGB_SUBA_PRIM_COLOR|
		CC_C0_RGB_SUBB_ZERO_COLOR|CC_C1_RGB_SUBB_ZERO_COLOR|
		CC_C0_RGB_MUL_TEXEL0_COLOR|CC_C1_RGB_MUL_TEXEL0_COLOR|
		CC_C0_RGB_ADD_ZERO_COLOR|CC_C1_RGB_ADD_ZERO_COLOR| 
		CC_C0_ALPHA_MUL_ZERO|CC_C0_ALPHA_ADD_PRIM|
		CC_C1_ALPHA_MUL_ZERO|CC_C1_ALPHA_ADD_PRIM);

	rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_1CYCLE|MODE_ALPHA_COMPARE_EN|MODE_CVG_DEST_CLAMP|MODE_FORCE_BLEND|MODE_RGB_DITHER_SEL_NONE|GBL_c1(G_BL_CLR_IN, G_BL_0, G_BL_CLR_IN, G_BL_1)|GBL_c2(G_BL_CLR_IN, G_BL_0, G_BL_CLR_IN, G_BL_1));

	KateGL::textureManager.CacheTexture(&display_list_current, tex);	
	rdp_draw_textured_rectangle_scaled(&display_list_current, TEXSLOT_0, x, y, x+(8 * scaleX), y+(8 * scaleY), scaleX, scaleY, 0, 0);
	rdp_sync(&display_list_current, SYNC_FULL);
	rdp_end_display_list(&display_list_current);

	run_ucode();
	wait_for_rsp();
	wait_for_rdp();
}