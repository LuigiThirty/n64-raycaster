#pragma once

#include "assets/textures/textures.h"
#include <kategl/kategl.hpp>

typedef struct {
    Point2D coord;
    Point2D size;
    char texture_name[10];
} Font_AtlasEntry;

typedef struct {
    char font_name[10];
    Font_AtlasEntry atlastable[256];
} Font_Atlas;

Font_Atlas atlas_ariallower;

void setup_arial_font()
{
	strcpy(atlas_ariallower.font_name, "ARIALLO");

	atlas_ariallower.atlastable['a'].coord = {.x = 0, .y = 0};
	atlas_ariallower.atlastable['a'].size = {.x = 7, .y = 9};
	strcpy(atlas_ariallower.atlastable['a'].texture_name, TEX_F_ARIAL1);

	atlas_ariallower.atlastable['b'].coord = {.x = 10, .y = 0};
	atlas_ariallower.atlastable['b'].size = {.x = 7, .y = 9};
	strcpy(atlas_ariallower.atlastable['b'].texture_name, TEX_F_ARIAL1);

	atlas_ariallower.atlastable['c'].coord = {.x = 20, .y = 0};
	atlas_ariallower.atlastable['c'].size = {.x = 7, .y = 9};
	strcpy(atlas_ariallower.atlastable['c'].texture_name, TEX_F_ARIAL1);

	atlas_ariallower.atlastable['d'].coord = {.x = 29, .y = 0};
	atlas_ariallower.atlastable['d'].size = {.x = 7, .y = 9};
	strcpy(atlas_ariallower.atlastable['d'].texture_name, TEX_F_ARIAL1);

	atlas_ariallower.atlastable['e'].coord = {.x = 39, .y = 0};
	atlas_ariallower.atlastable['e'].size = {.x = 7, .y = 9};
	strcpy(atlas_ariallower.atlastable['e'].texture_name, TEX_F_ARIAL1);

	atlas_ariallower.atlastable['f'].coord = {.x = 49, .y = 0};
	atlas_ariallower.atlastable['f'].size = {.x = 4, .y = 9};
	strcpy(atlas_ariallower.atlastable['f'].texture_name, TEX_F_ARIAL1);

	atlas_ariallower.atlastable['g'].coord = {.x = 55, .y = 0};
	atlas_ariallower.atlastable['g'].size = {.x = 7, .y = 11};
	strcpy(atlas_ariallower.atlastable['g'].texture_name, TEX_F_ARIAL1);

	atlas_ariallower.atlastable['h'].coord = {.x = 0, .y = 0};
	atlas_ariallower.atlastable['h'].size = {.x = 7, .y = 11};
	strcpy(atlas_ariallower.atlastable['h'].texture_name, TEX_F_ARIAL2);

	atlas_ariallower.atlastable['i'].coord = {.x = 10, .y = 0};
	atlas_ariallower.atlastable['i'].size = {.x = 3, .y = 11};
	strcpy(atlas_ariallower.atlastable['i'].texture_name, TEX_F_ARIAL2);

	atlas_ariallower.atlastable['j'].coord = {.x = 15, .y = 0};
	atlas_ariallower.atlastable['j'].size = {.x = 4, .y = 11};
	strcpy(atlas_ariallower.atlastable['j'].texture_name, TEX_F_ARIAL2);

	atlas_ariallower.atlastable['k'].coord = {.x = 22, .y = 0};
	atlas_ariallower.atlastable['k'].size = {.x = 7, .y = 11};
	strcpy(atlas_ariallower.atlastable['k'].texture_name, TEX_F_ARIAL2);

	atlas_ariallower.atlastable['l'].coord = {.x = 31, .y = 0};
	atlas_ariallower.atlastable['l'].size = {.x = 3, .y = 11};
	strcpy(atlas_ariallower.atlastable['l'].texture_name, TEX_F_ARIAL2);

	atlas_ariallower.atlastable['m'].coord = {.x = 37, .y = 0};
	atlas_ariallower.atlastable['m'].size = {.x = 10, .y = 11};
	strcpy(atlas_ariallower.atlastable['m'].texture_name, TEX_F_ARIAL2);

	atlas_ariallower.atlastable['n'].coord = {.x = 50, .y = 0};
	atlas_ariallower.atlastable['n'].size = {.x = 7, .y = 11};
	strcpy(atlas_ariallower.atlastable['n'].texture_name, TEX_F_ARIAL2);

	atlas_ariallower.atlastable['o'].coord = {.x = 0, .y = 0};
	atlas_ariallower.atlastable['o'].size = {.x = 7, .y = 11};
	strcpy(atlas_ariallower.atlastable['o'].texture_name, TEX_F_ARIAL3);

	atlas_ariallower.atlastable['p'].coord = {.x = 10, .y = 0};
	atlas_ariallower.atlastable['p'].size = {.x = 7, .y = 11};
	strcpy(atlas_ariallower.atlastable['p'].texture_name, TEX_F_ARIAL3);

	atlas_ariallower.atlastable['q'].coord = {.x = 20, .y = 0};
	atlas_ariallower.atlastable['q'].size = {.x = 7, .y = 11};
	strcpy(atlas_ariallower.atlastable['q'].texture_name, TEX_F_ARIAL3);

	atlas_ariallower.atlastable['r'].coord = {.x = 30, .y = 0};
	atlas_ariallower.atlastable['r'].size = {.x = 5, .y = 11};
	strcpy(atlas_ariallower.atlastable['r'].texture_name, TEX_F_ARIAL3);

	atlas_ariallower.atlastable['s'].coord = {.x = 37, .y = 0};
	atlas_ariallower.atlastable['s'].size = {.x = 6, .y = 11};
	strcpy(atlas_ariallower.atlastable['s'].texture_name, TEX_F_ARIAL3);

	atlas_ariallower.atlastable['t'].coord = {.x = 46, .y = 0};
	atlas_ariallower.atlastable['t'].size = {.x = 4, .y = 11};
	strcpy(atlas_ariallower.atlastable['t'].texture_name, TEX_F_ARIAL3);

	atlas_ariallower.atlastable['u'].coord = {.x = 53, .y = 0};
	atlas_ariallower.atlastable['u'].size = {.x = 7, .y = 11};
	strcpy(atlas_ariallower.atlastable['u'].texture_name, TEX_F_ARIAL3);

	atlas_ariallower.atlastable['v'].coord = {.x = 0, .y = 0};
	atlas_ariallower.atlastable['v'].size = {.x = 7, .y = 9};
	strcpy(atlas_ariallower.atlastable['v'].texture_name, TEX_F_ARIAL4);

	atlas_ariallower.atlastable['w'].coord = {.x = 9, .y = 0};
	atlas_ariallower.atlastable['w'].size = {.x = 9, .y = 9};
	strcpy(atlas_ariallower.atlastable['w'].texture_name, TEX_F_ARIAL4);

	atlas_ariallower.atlastable['x'].coord = {.x = 21, .y = 0};
	atlas_ariallower.atlastable['x'].size = {.x = 7, .y = 9};
	strcpy(atlas_ariallower.atlastable['x'].texture_name, TEX_F_ARIAL4);

	atlas_ariallower.atlastable['y'].coord = {.x = 30, .y = 0};
	atlas_ariallower.atlastable['y'].size = {.x = 7, .y = 9};
	strcpy(atlas_ariallower.atlastable['y'].texture_name, TEX_F_ARIAL4);

	atlas_ariallower.atlastable['z'].coord = {.x = 39, .y = 0};
	atlas_ariallower.atlastable['z'].size = {.x = 7, .y = 9};
	strcpy(atlas_ariallower.atlastable['z'].texture_name, TEX_F_ARIAL4);
}