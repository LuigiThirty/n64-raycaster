#pragma once

#include <64drive.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <libdragon.h>

#include "mmio.h"
#include "regs.h"
#include "microcode/basic.h"

extern const char basic_ucode_start;
extern const char overlay_drawtri_ucode_start;

typedef struct {
	char name[8];
	void *address;
	uint16_t load_address;
	uint16_t length;
} program_segment_t;

// TODO: put this in RSPSegmentManager
extern program_segment_t segments[8];

class RSPSegmentManager
{
	public:
	static void InitSegmentsList();
	static void CopySegmentAddressesToDMEM();
	static void LoadBootSegment();
};

