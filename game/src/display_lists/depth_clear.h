#include <libdragon.h>
#include "microcode/basic.h"

#ifdef __cplusplus
extern "C" {
#endif

extern uint16_t z_buffer[320*240];
extern char *display_buffer;

void DL_ClearDepthBuffer(display_context_t context);
void DL_ClearFrameBuffer(display_context_t context);

#ifdef __cplusplus
}
#endif