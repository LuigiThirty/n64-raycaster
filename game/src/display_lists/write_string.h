#include <libdragon.h>
#include "microcode/basic.h"

#ifdef __cplusplus
extern "C" {
#endif

extern uint16_t z_buffer[320*240];

void DL_WriteString(display_context_t context, char *str, char *fontname, int x, int y, float scaleX, float scaleY);

#ifdef __cplusplus
}
#endif