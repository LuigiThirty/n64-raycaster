#include "depth_clear.h"
#include "stdio.h"
#include "kategl/kategl.hpp"

void DL_ClearDepthBuffer(display_context_t context)
{
	display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

	rdp_set_default_clipping(&display_list_current);
	rdp_set_default_clipping(&display_list_current);
	rdp_set_default_clipping(&display_list_current);
	rdp_set_default_clipping(&display_list_current);

    // Clear the depth buffer.
	rdp_set_color_image(&display_list_current, RDP_IMAGE_RGBA, RDP_PIXEL_16BIT, 319, (uint16_t *)z_buffer);
	rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_FILL);
	rdp_set_fill_color(&display_list_current, 0xFFFCFFFC);
	rdp_sync(&display_list_current, SYNC_PIPE);
	rdp_draw_filled_rectangle(&display_list_current, 0, 0, 319, 239);

    rdp_sync(&display_list_current, SYNC_FULL);
}

void DL_ClearFrameBuffer(display_context_t context)
{
	display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

	rdp_set_default_clipping(&display_list_current);
	rdp_set_default_clipping(&display_list_current);
	rdp_set_default_clipping(&display_list_current);
	rdp_set_default_clipping(&display_list_current);

    // Clear the frame buffer.
	KateGL::DL::klUseDisplayContext(&display_list_current, context);
	rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_FILL);
	rdp_set_fill_color(&display_list_current, 0x00000000);
	rdp_sync(&display_list_current, SYNC_PIPE);
	rdp_draw_filled_rectangle(&display_list_current, 0, 0, 319, 239);

    rdp_sync(&display_list_current, SYNC_FULL);
}