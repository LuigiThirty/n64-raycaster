#include "write_string.h"
#include "macros.h"
#include "kategl/kategl.hpp"

char dbg[512];

void DL_WriteString(display_context_t context, char *str, char *fontname, int x, int y, float scaleX, float scaleY)
{
	KateGL::TextureListEntry *texUp = KateGL::textureManager.GetTextureByName("F_KRN_UP");
	KateGL::TextureListEntry *texLo = KateGL::textureManager.GetTextureByName("F_KRN_LO");
	KateGL::TextureListEntry *texNo = KateGL::textureManager.GetTextureByName("F_KRN_NO");

	// Groups of 8 letters.
	int groups = (strlen(str) / 8)+1;
	int length = strlen(str);

	for(auto group=0; group<groups; group++)
	{
		display_list_t *display_list_current = (display_list_t *)BASIC_InputDisplayList;

		KateGL::DL::klPipeSync(&display_list_current);
		KateGL::DL::klPreamble(&display_list_current);
		KateGL::DL::klUseDisplayContext(&display_list_current, context);
		// KateGL::DL::klCombineModeTexturedRectangle(&display_list_current);

		rdp_set_primitive_color(&display_list_current, 0xFFFFFFFF);
		rdp_set_combine_mode(&display_list_current,
			CC_C0_RGB_SUBA_PRIM_COLOR|CC_C1_RGB_SUBA_PRIM_COLOR|
			CC_C0_RGB_SUBB_ZERO_COLOR|CC_C1_RGB_SUBB_ZERO_COLOR|
			CC_C0_RGB_MUL_TEXEL0_COLOR|CC_C1_RGB_MUL_TEXEL0_COLOR|
			CC_C0_RGB_ADD_ZERO_COLOR|CC_C1_RGB_ADD_ZERO_COLOR| 
			CC_C0_ALPHA_MUL_ZERO|CC_C0_ALPHA_ADD_PRIM|
			CC_C1_ALPHA_MUL_ZERO|CC_C1_ALPHA_ADD_PRIM);

		rdp_set_other_modes(&display_list_current, MODE_CYCLE_TYPE_1CYCLE|MODE_ALPHA_COMPARE_EN|MODE_CVG_DEST_CLAMP|MODE_FORCE_BLEND|MODE_RGB_DITHER_SEL_NONE|GBL_c1(G_BL_CLR_IN, G_BL_0, G_BL_CLR_IN, G_BL_1)|GBL_c2(G_BL_CLR_IN, G_BL_0, G_BL_CLR_IN, G_BL_1));

		for(int i=0; i<8; i++)
		{
			uint8_t c = str[i + (group*8)] - 0x20;
			int row = ((c & 0x1F) >> 4) * 8;
			int col = (c & 0x0F) * 8;

			if(c == 0)
			{
				x += 8;
				continue;
			}
			else if(c <= 0x1F)
			{
				KateGL::textureManager.CacheTexture(&display_list_current, texNo);
			}
			else if(c >= 0x20 && c <= 0x3F)
			{
				KateGL::textureManager.CacheTexture(&display_list_current, texUp);
			}
			else if(c <= 0x5F)
			{
				KateGL::textureManager.CacheTexture(&display_list_current, texLo);
			}
			else
			{
				break;
			}
			
			KateGL::DL::klPipeSync(&display_list_current);
			rdp_draw_textured_rectangle_scaled(&display_list_current, TEXSLOT_0, x, y, x+(8 * scaleX), y+(8 * scaleY), scaleX, scaleY, (col<<5), (row<<5));
			x += (8 * scaleX);
		}

		rdp_sync(&display_list_current, SYNC_FULL);
		rdp_end_display_list(&display_list_current);

		run_ucode();
		wait_for_rsp();
		wait_for_rdp();

		KateGL::textureManager.ClearCachedTexture();
	}
}