#pragma once

#include <libdragon.h>
#include <stdio.h>
#include <kategl/types.hpp>


// All PAK data is stored LITTLE-ENDIAN.
class Pak
{
    private:
    #pragma pack(push,1)
    typedef struct {
        uint32_t id;
        uint32_t file_table_offset;
        uint32_t file_table_size;
    } Header;

    Header header;
    uint8_t *data = NULL;

    typedef struct {
        char        name[56];           // File name.
        uint32_t    starting_offset;    // Where in the WAD file does the lump begin?
        uint32_t    length;             // Length of the lump's data.
    } FileRecord;
    #pragma pop

    public:

    // Load a pak file into RDRAM from DragonFS.
    void LoadFromDFS(char *pak_filename);

    // Get a pointer to the file's data.
    uint8_t *GetFileDataPtr(char *filename);

    // Get the file's size.
    uint32_t GetFileSize(char *filename);

    // Unload the PAK file.
    void Unload();
};