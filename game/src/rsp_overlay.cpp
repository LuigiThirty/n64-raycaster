#include "rsp_overlay.h"

program_segment_t segments[8];

void RSPSegmentManager::InitSegmentsList()
{
    // Boot segment
    strcpy(segments[0].name, "BOOT");
    segments[0].address = (void *)&basic_ucode_start;
    segments[0].load_address = 0x000;
    segments[0].length = 0x1000;

    // DrawTriangle segment
    strcpy(segments[1].name, "DRAWTRI");
    segments[1].address = ((uint8_t *)&overlay_drawtri_ucode_start)+0x400;
    segments[1].load_address = 0x400;
    segments[1].length = 0xC00;
}

void RSPSegmentManager::CopySegmentAddressesToDMEM()
{
	// Populate DMEM with the addresses of the overlays.
	MMIO32(SP_DMEM_BASE + 0x800) = (uint32_t)(segments[0].address);
	MMIO32(SP_DMEM_BASE + 0x804) = (uint32_t)(segments[1].address);
}

void RSPSegmentManager::LoadBootSegment()
{
	_64Drive_putstring("Loading boot microcode...\n");
	load_ucode(segments[0].address, segments[0].length);
}