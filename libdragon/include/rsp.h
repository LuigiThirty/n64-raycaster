/**
 * @file rsp.h
 * @brief Hardware Vector Interface
 * @ingroup rsp
 */
#ifndef __LIBDRAGON_RSP_H
#define __LIBDRAGON_RSP_H

#ifdef __cplusplus
extern "C" {
#endif

void rsp_init();
void load_ucode(void * start, unsigned long size);
void read_ucode(void* start, unsigned long size);
void run_ucode();

extern volatile uint32_t rsp_active;
extern volatile uint32_t rdp_status;
extern volatile bool waiting_for_rdp;

void wait_for_rsp();
void wait_for_rdp();

#ifdef __cplusplus
}
#endif

#endif
