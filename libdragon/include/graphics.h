/**
 * @file graphics.h
 * @brief 2D Graphics
 * @ingroup graphics
 */
#ifndef __LIBDRAGON_GRAPHICS_H
#define __LIBDRAGON_GRAPHICS_H

#include "display.h"

/**
 * @addtogroup graphics
 * @{
 */

/** @brief Generic color structure */
typedef struct
{
    /** @brief Red component */
    uint8_t r;
    /** @brief Green component */
    uint8_t g;
    /** @brief Blue component */
    uint8_t b;
    /** @brief Alpha component */
    uint8_t a;
} color_t;

/** @brief Texture format */
typedef enum
{
    KL_TEX_FORMAT_RGBA,
    KL_TEX_FORMAT_YUV,
    KL_TEX_FORMAT_CI,
    KL_TEX_FORMAT_IA,
    KL_TEX_FORMAT_I
} KLTextureFormat;

typedef enum
{
    KL_TEX_DEPTH_4BIT   = 0,
    KL_TEX_DEPTH_8BIT   = 1,
    KL_TEX_DEPTH_16BIT  = 2,
    KL_TEX_DEPTH_32BIT  = 4,
} KLTextureDepth;

typedef struct {
    int16_t x, y;
} Point2D;
// static Point2D MakePoint(int x, int y) { Point2D p; p.x = x; p.y = y; return p; }

  /** @brief Bitmap structure */
typedef struct
{
    /** @brief Dimensions in pixels */
    Point2D size;

    /** 
     * @brief Bit depth expressed in bytes
     *
     * A 32 bit sprite would have a value of '4' here
     */
    uint8_t bitdepth;

    /** 
     * @brief Texture format
     */
    KLTextureFormat format;

    /** 
     * @brief Texture pixel size
     */
    KLTextureDepth pixel_size;

    /** @brief Number of horizontal slices for spritemaps */
    uint8_t hslices;
    /** @brief Number of vertical slices for spritemaps */
    uint8_t vslices;

    /** @brief Start of graphics data */
    void *data;

    /** @brief TLUT pointer, if TLUT exists */
    uint16_t *tlut;

    /** @brief Number of colors in the TLUT. If no TLUT, this should be 0. */
    uint8_t tlut_colors;

} KLBitmap;

#ifdef __cplusplus
extern "C" {
#endif

uint32_t graphics_make_color( int r, int g, int b, int a );
uint32_t graphics_convert_color( color_t color );
void graphics_draw_pixel( display_context_t disp, int x, int y, uint32_t c );
void graphics_draw_pixel_trans( display_context_t disp, int x, int y, uint32_t c );
void graphics_draw_line( display_context_t disp, int x0, int y0, int x1, int y1, uint32_t c );
void graphics_draw_line_trans( display_context_t disp, int x0, int y0, int x1, int y1, uint32_t c );
void graphics_draw_box( display_context_t disp, int x, int y, int width, int height, uint32_t color );
void graphics_draw_box_trans( display_context_t disp, int x, int y, int width, int height, uint32_t color );
void graphics_fill_screen( display_context_t disp, uint32_t c );
void graphics_set_color( uint32_t forecolor, uint32_t backcolor );
void graphics_draw_character( display_context_t disp, int x, int y, char c );
void graphics_draw_text( display_context_t disp, int x, int y, const char * const msg );
void graphics_draw_sprite( display_context_t disp, int x, int y, KLBitmap *sprite );
void graphics_draw_sprite_stride( display_context_t disp, int x, int y, KLBitmap *sprite, int offset );
void graphics_draw_sprite_trans( display_context_t disp, int x, int y, KLBitmap *sprite );
void graphics_draw_sprite_trans_stride( display_context_t disp, int x, int y, KLBitmap *sprite, int offset );

#ifdef __cplusplus
}
#endif

/** @} */ /* graphics */

#endif
