#pragma once

#include "stdint.h"

typedef int32_t Fixed;

static uint32_t SwapEndiannessUInt32(uint32_t val)
{
    return ((val & 0xFF000000) >> 24) | ((val & 0x00FF0000) >> 8) | ((val & 0x0000FF00) << 8) | ((val & 0x000000FF) << 24);
}

namespace KateGL
{
    typedef uint16_t TEXTURE_TLUT[];
    typedef uint8_t TEXTURE_8[];
    typedef uint16_t TEXTURE_16[];

    template <typename T>
    struct Vector2
    {
        T x, y;

        Vector2<T>()
        {
            x = 0;
            y = 0;
        }        
        
        Vector2<T>(T _x, T _y)
        {
            x = _x;
            y = _y;
        }
    };

    template <typename T>
    struct Vector3 
    {
        T x, y, z;

        Vector3<T>()
        {
            x = 0;
            y = 0;
            z = 0;
        }       
        
        Vector3<T>(T _x, T _y, T _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }
    };

    typedef Vector3<Fixed> Point3D;
}