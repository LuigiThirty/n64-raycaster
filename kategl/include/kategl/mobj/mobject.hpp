#ifndef KATEGL_MOBJ_MOBJECT_H
#define KATEGL_MOBJ_MOBJECT_H

#include <libdragon.h>
#include "kategl/gameobj.hpp"
#include "kategl/types.hpp"

#define MAX_MOBJECTS 256

namespace KateGL
{
    /* a Motion Object in the game world. */
    class MObject: public GameObject {
        protected:
        int id;

        public:
        // Instance variables
        Vector3<Fixed> position;
        Vector3<Fixed> velocity;

        // Methods
        MObject();

        // virtual void AdvanceAnimationVblank() = 0;
        void ExecDisplayList(display_context_t context, int mobj_id, bool all) {};
        static void DrawAll(display_context_t context) {};

        void DoMovement()
        {
            position.x += velocity.x;
            position.y += velocity.y;
            position.z += velocity.z;
        }
    };

}

#endif