#ifndef KATEGL_BOUND_H
#define KATEGL_BOUND_H

#include "kategl/types.hpp"
#include "kategl/texture/texture.h"

namespace KateGL
{
    bool PointIsInsideTextureBound2D(TextureListEntry *tle, Point3D point_a, Point3D target);
    bool TexelContainsBackgroundPixel(TextureListEntry *tle, Point2D texel);
}

#endif