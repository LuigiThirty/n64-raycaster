#pragma once

#include <stdint.h>

namespace KateGL
{
    #pragma pack(push,1)
    typedef struct Vertex {
        int16_t 	x, y, z;	// Vertex coordinates (signed 16-bit integer)
        int16_t     w;
        uint8_t		r, g, b, a; // Colors and alpha (0-255, unsigned 8-bit)
        uint16_t 	s, t;		// Texture coordinates (S10.5 fixed-point)
    } BASIC_Vertex;				// Length: 16 bytes
    #pragma pack(pop)

    typedef uint16_t Triangle[3];

    class Mesh
    {
        public:
        Vertex *vertices;
        Triangle *triangles;

        int vertexCount;
        int triangleCount;

        Mesh(Vertex *_verts, int _numVerts, Triangle *_tris, int _numTris)
        {
            vertices = _verts;
            vertexCount = _numVerts;

            triangles = _tris;
            triangleCount = _numTris;
        }
    };
}