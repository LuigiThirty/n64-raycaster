#ifndef KATEGL_MOBJ_POLYOBJECT_H
#define KATEGL_MOBJ_POLYOBJECT_H

#include <libdragon.h>
#include "kategl/polyobj/mesh.hpp"
#include "kategl/gameobj.hpp"
#include "kategl/types.hpp"
#include "kategl/matrix.hpp"
#include "kategl/transform.hpp"
#include <stdio.h>

#define MAX_POLYOBJECTS 256

namespace KateGL
{
    class PolyObject: public GameObject
    {
        protected:
        // These three attributes will construct a model matrix.
        Vector3<Fixed> position = {0, 0, 0};
        Vector3<float> rotation = {0, 0, 0};
        Vector3<float> scale = {1, 1, 1};

        // ?
        Vector3<float> velocity;

        // The polygon mesh for the PolyObject.
        Mesh *mesh;
                
        PolyObject()
        {
        };

        public:
        Vector3<Fixed> GetPosition() { return position; }
        Vector3<float> GetRotation() { return rotation; }
        Vector3<float> GetScale() { return scale; }

        Mesh * GetMesh() { return mesh; }

        void SetRotation(Vector3<float> rot) { rotation = rot; }
        void SetRotation(float x, float y, float z) { this->rotation.x = x; this->rotation.y = y; this->rotation.z = z; }

        void SetTranslation(Vector3<Fixed> pos) { position = pos; }
        void SetTranslation(Fixed x, Fixed y, Fixed z) { this->position.x = x; this->position.y = y; this->position.z = z; }

        void SetScale(Vector3<float> scl) { scale = scl; }
        void SetScale(float x, float y, float z) { this->scale.x = x; this->scale.y = y; this->scale.z = z; }
    
        void ConstructMatrix(FX_Matrix44 *mtx);
    };
}

#endif