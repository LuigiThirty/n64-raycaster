#pragma once

/* Fixed-point matrix. */

#include "math.h"
#include "stdint.h"
#include "stdlib.h"
#include "fixed.h"

#define MMIO8(x)  (*(volatile uint8_t *)(x))
#define MMIO16(x) (*(volatile uint16_t *)(x))
#define MMIO32(x) (*(volatile uint32_t *)(x))

namespace KateGL
{
    typedef struct FX_Matrix44 { 
        Fixed data[4][4]; 

        FX_Matrix44()
        {
            for(int i=0; i<4; i++)
            {
                for(int j=0; j<4; j++)
                {
                    data[i][j] = 0;
                }
            }
        }

        FX_Matrix44 operator=(const FX_Matrix44 &m)
        {
            this->data[0][0] = m.data[0][0]; 
            this->data[0][1] = m.data[0][1]; 
            this->data[0][2] = m.data[0][2]; 
            this->data[0][3] = m.data[0][3]; 

            this->data[1][0] = m.data[1][0]; 
            this->data[1][1] = m.data[1][1]; 
            this->data[1][2] = m.data[1][2]; 
            this->data[1][3] = m.data[1][3];

            this->data[2][0] = m.data[2][0]; 
            this->data[2][1] = m.data[2][1]; 
            this->data[2][2] = m.data[2][2]; 
            this->data[2][3] = m.data[2][3];

            this->data[3][0] = m.data[3][0]; 
            this->data[3][1] = m.data[3][1]; 
            this->data[3][2] = m.data[3][2]; 
            this->data[3][3] = m.data[3][3];

            return *this;
        }

        FX_Matrix44(const FX_Matrix44 &m)
        {
            this->data[0][0] = m.data[0][0]; 
            this->data[0][1] = m.data[0][1]; 
            this->data[0][2] = m.data[0][2]; 
            this->data[0][3] = m.data[0][3]; 

            this->data[1][0] = m.data[1][0]; 
            this->data[1][1] = m.data[1][1]; 
            this->data[1][2] = m.data[1][2]; 
            this->data[1][3] = m.data[1][3];

            this->data[2][0] = m.data[2][0]; 
            this->data[2][1] = m.data[2][1]; 
            this->data[2][2] = m.data[2][2]; 
            this->data[2][3] = m.data[2][3];

            this->data[3][0] = m.data[3][0]; 
            this->data[3][1] = m.data[3][1]; 
            this->data[3][2] = m.data[3][2]; 
            this->data[3][3] = m.data[3][3];
        }
    } FX_Matrix44;
    
    class PolyObject;

	void klConstructTRSMatrix(PolyObject *obj, FX_Matrix44 *out);
    void klUnitMatrix(FX_Matrix44 *m);
    void FXM44_CopyToDMEM(uint32_t *addr, FX_Matrix44 *m);
    void klMatrixMultiply(FX_Matrix44 *a, FX_Matrix44 *b, FX_Matrix44 *result);

    extern void thanksIHateFortran(int *num, int *o);
}