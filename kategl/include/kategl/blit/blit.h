#pragma once

#include "stdint.h"
#include "graphics.h"

void klBitBlt(KLBitmap *destBitmap, int destX, int destY, KLBitmap *sourceBitmap, int sourceX, int sourceY, int sizeX, int sizeY);