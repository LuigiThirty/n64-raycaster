#pragma once

#include <math.h>
#include "kategl/matrix.hpp"
#include "kategl/types.hpp"

namespace KateGL
{
  typedef float Radians;

  float DegreesToRadians(float degrees);

  typedef struct {
    Vector2<float> position;
    float rotation; // 2D rotation in degrees
  } Transform2D;

  Vector2<float> RotationFromT2DXY(const Transform2D transform, const float x, const float y);

  void klTranslation(FX_Matrix44 *m, Fixed x, Fixed y, Fixed z);
  void klRotationDegreesX(FX_Matrix44 *m, int degrees);
  void klRotationDegreesY(FX_Matrix44 *m, int degrees);
  void klRotationDegreesZ(FX_Matrix44 *m, int degrees);

  void klScale(FX_Matrix44 *m, Fixed x, Fixed y, Fixed z);
}