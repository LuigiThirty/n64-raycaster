#ifndef KATEGL_TEXTURE_H
#define KATEGL_TEXTURE_H

#include "kategl/texture/loader.h"
#include "kategl/texture/manager.h"

#define TEXTURE_CI_SCALE_X 0.25
#define TEXTURE_CI_SCALE_Y 1.0

#endif