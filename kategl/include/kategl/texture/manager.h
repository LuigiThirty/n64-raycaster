#pragma once

// Texture Manager
#include <libdragon.h>
#include <stdio.h>
#include "kategl/blit/blit.h"

namespace KateGL
{
    typedef struct texture_list_entry_t {
        struct texture_list_entry_t *next;
        char name[10];
        KLBitmap *info;
    } TextureListEntry;

    //TextureListEntry *textureList;

    class TextureManager
    {
        private:
        TextureListEntry *list;
        TextureListEntry *cachedTexture; // The texture currently in TMEM.

        public:
        void AppendToTextureList(const char *name, void *data, int sizeX, int sizeY, KLTextureFormat format, KLTextureDepth pixel_size, uint16_t *tlut, uint16_t tlut_colors);
        void AppendToTextureList(TextureListEntry *texture);
        int TextureCount();
        TextureListEntry* GetTextureByName(const char *seekname);

        void ClearCachedTexture() { cachedTexture = NULL; }
        void SetCachedTexture(TextureListEntry *loaded) { cachedTexture = loaded; data_cache_hit_writeback_invalidate(&cachedTexture, sizeof(cachedTexture)); }
        TextureListEntry *GetCachedTexture() { return cachedTexture; }
        
        // RDP command macros.
        display_list_t* CacheTexture(DLPtr *list, TextureListEntry *tle);
    };

    extern TextureManager textureManager;
}

