#pragma once

#include <libdragon.h>
#include <stdio.h>
#include "kategl/blit/blit.h"

void TEX_LoadTextureFromCHeader(KLBitmap *tex, unsigned short *image, uint16_t width, uint16_t height, KLTextureDepth texture_size, KLTextureFormat texture_format);
void TEX_LoadTextureFromFS(KLBitmap *tex, char *filename, uint16_t width, uint16_t height, KLTextureDepth texture_size, KLTextureFormat texture_format);
