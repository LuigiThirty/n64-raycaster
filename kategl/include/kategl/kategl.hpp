#pragma once

#include "kategl/mobj/mobject.hpp"
#include "kategl/polyobj/polyobj.hpp"
#include "kategl/texture/texture.h"
#include "kategl/bound.hpp"
#include "kategl/fixed.h"
#include "kategl/highlevel.hpp"
#include "kategl/matrix.hpp"
#include "kategl/perspective.hpp"
#include "kategl/transform.hpp"
#include "kategl/types.hpp"
