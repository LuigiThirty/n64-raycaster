#ifndef KATEGL_HIGHLEVEL_H
#define KATEGL_HIGHLEVEL_H

/* High-level API commands for KateGL. */

#include <libdragon.h>
#include "kategl/mobj/mobject.hpp"
#include "kategl/texture/texture.h"
#define UNCACHED_ADDR(x)    ((void *)(((uint32_t)(x)) | 0xA0000000))

namespace KateGL
{
    namespace DL
    {
        void klPreamble(DLPtr *list);
        void klDrawTexturedMObject(DLPtr *list, MObject *mobj, TextureListEntry *texture);
        void klSyncAndEnd(DLPtr *list);

        void klLoadSync(DLPtr *list);
        void klPipeSync(DLPtr *list);
        void klTileSync(DLPtr *list);
        void klFullSync(DLPtr *list);
        
        void klUseDisplayContext(DLPtr *list, display_context_t context);
        void klCombineModeTexturedRectangle(DLPtr *list);
        void klSetupForMObject(DLPtr *list);

        DLPtr klGetBufferedDisplayList();
        void klExecuteBufferedDisplayList(void *rdp_addr);
    }
}

#endif
