#include <libdragon.h>
#include "kategl/matrix.hpp"
#include "math.h"

namespace KateGL
{
    void klPerspective(FX_Matrix44 *m, float near_plane, float far_plane);
}
