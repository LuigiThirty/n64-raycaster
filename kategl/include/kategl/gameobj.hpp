#pragma once

namespace KateGL
{
    typedef enum team_t {
        TEAM_NULL,
        TEAM_GOOD,
        TEAM_BAD
    } MObjectTeam;

    enum state_t {
        STATE_NULL
    };

    class GameObject {
        protected:
        int id;

        public:
        MObjectTeam team = TEAM_NULL;
        int state = STATE_NULL;

        int GetID() { return id; }
        void SetID(int _id) { id = _id; }
    };
   
}
