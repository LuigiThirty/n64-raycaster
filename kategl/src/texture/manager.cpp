#include "kategl/texture/manager.h"

namespace KateGL
{

    TextureManager textureManager;

    void TextureManager::AppendToTextureList(const char *name, void *data, 
        int sizeX, int sizeY, 
        KLTextureFormat format, KLTextureDepth pixel_size,
        uint16_t *tlut, uint16_t tlut_colors)
    {
        KLBitmap *bmp = new KLBitmap;
        bmp->bitdepth = 4;
        bmp->data = data;
        bmp->size.x = sizeX;
        bmp->size.y = sizeY;
        bmp->format = format;
        bmp->pixel_size = pixel_size;
        bmp->tlut = tlut;
        bmp->tlut_colors = tlut_colors;

        TextureListEntry *tle = new TextureListEntry;
        strcpy(tle->name, name);
        tle->info = bmp;

        this->AppendToTextureList(tle);
    }

    void TextureManager::AppendToTextureList(TextureListEntry *texture)
    {
        texture->next = NULL;

        if(list == NULL)
        {
            list = texture;
        }
        else
        {
            TextureListEntry *current = list;

            while(current->next != NULL)
            {
                current = current->next;
            }
            current->next = texture;
        }
        
    }

    int TextureManager::TextureCount()
    {
        if(list == NULL)
        {
            return -1; // uninitialized list
        }
        else
        {
            TextureListEntry *current = list;
            int count = 0;

            while(current != NULL)
            {
                current = current->next;
                count++;
            }

            return count;
        }
    }

    TextureListEntry* TextureManager::GetTextureByName(const char *seekname)
    {
        if(list == NULL)
        {
            return NULL;
        }
        else
        {
            TextureListEntry *current = list;
            while(current != NULL)
            {
                if(strcmp(current->name, seekname) == 0) return current;

                current = current->next;
            }

            // Didn't find it. Write an error to the console.
            char buf[64];
            sprintf(buf, "Texture '%s' not found in texture list %p\n", seekname, list);
            _64Drive_putstring(buf);
            return NULL;
        }
    }

    display_list_t* TextureManager::CacheTexture(DLPtr *list, TextureListEntry *tle)
    {
        // Is this texture already cached? If so, ignore this command.
        if(cachedTexture == tle) return *list;

        // Is there a TLUT?
        if(tle->info->tlut != NULL)
        {
            // A TLUT is always RGBA 16-bit.
            rdp_set_texture_image(list, RDP_IMAGE_RGBA, RDP_PIXEL_16BIT, 0, (uint16_t *)tle->info->tlut);
            rdp_sync(list, SYNC_PIPE);
            rdp_set_tile_for_tlut(list, tle->info->tlut_colors, 0x800, TEXSLOT_0);
            rdp_load_tlut(list, tle->info->tlut_colors, 0, TEXSLOT_0);
        }

        // Wait for the previous load to complete, then perform this load.
        rdp_load_texture(list, TEXSLOT_0, 0x000, MIRROR_DISABLED, tle->info);

        SetCachedTexture(tle);

        return *list; // list is now advanced to the end of the commands we added
    }

}