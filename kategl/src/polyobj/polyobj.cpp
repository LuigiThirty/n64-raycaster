#include "kategl/polyobj/polyobj.hpp"

namespace KateGL
{
    FX_Matrix44 translation, rotX, rotY, rotZ, scaleMtx;

    void PolyObject::ConstructMatrix(FX_Matrix44 *mtx)
    {
        char dbg[512];

        FX_Matrix44 out;
        klUnitMatrix((FX_Matrix44 *)UncachedAddr(&out));

        // Model
        // klTranslation((FX_Matrix44 *)UncachedAddr(&translation), FX_FromFloat(position.x), FX_FromFloat(position.y), FX_FromFloat(position.z));
        // klMatrixMultiply((FX_Matrix44 *)UncachedAddr(&translation), (FX_Matrix44 *)UncachedAddr(&out), (FX_Matrix44 *)UncachedAddr(&out));

        // klRotationDegreesX((FX_Matrix44 *)UncachedAddr(&rotX), 0);
        // klRotationDegreesY((FX_Matrix44 *)UncachedAddr(&rotY), 0);
        klRotationDegreesZ((FX_Matrix44 *)UncachedAddr(&rotZ), 45);
        klMatrixMultiply((FX_Matrix44 *)UncachedAddr(&rotZ), (FX_Matrix44 *)UncachedAddr(&out), (FX_Matrix44 *)UncachedAddr(&out));
        // klMatrixMultiply((FX_Matrix44 *)UncachedAddr(&rotY), (FX_Matrix44 *)UncachedAddr(&out), (FX_Matrix44 *)UncachedAddr(&out));
        // klMatrixMultiply((FX_Matrix44 *)UncachedAddr(&rotX), (FX_Matrix44 *)UncachedAddr(&out), (FX_Matrix44 *)UncachedAddr(&out));

        // klScale((FX_Matrix44 *)UncachedAddr(&scaleMtx), FX_FromFloat(scale.x), FX_FromFloat(scale.y), FX_FromFloat(scale.z));
        // klMatrixMultiply((FX_Matrix44 *)UncachedAddr(&scaleMtx), (FX_Matrix44 *)UncachedAddr(&out), (FX_Matrix44 *)UncachedAddr(&out));

        sprintf(dbg, "%08X %08X %08X %08X\n", out.data[0][0], out.data[0][1], out.data[0][2], out.data[0][3]);
        _64Drive_putstring(dbg);
        sprintf(dbg, "%08X %08X %08X %08X\n", out.data[1][0], out.data[1][1], out.data[1][2], out.data[1][3]);
        _64Drive_putstring(dbg);
        sprintf(dbg, "%08X %08X %08X %08X\n", out.data[2][0], out.data[2][1], out.data[2][2], out.data[2][3]);
        _64Drive_putstring(dbg);
        sprintf(dbg, "%08X %08X %08X %08X\n", out.data[3][0], out.data[3][1], out.data[3][2], out.data[3][3]);
        _64Drive_putstring(dbg);

        // Compose the transformation.
        memcpy(mtx, &out, sizeof(FX_Matrix44));
    }
}
