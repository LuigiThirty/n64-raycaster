#include "kategl/perspective.hpp"

namespace KateGL
{
	void klPerspective(FX_Matrix44 *m, float near_plane, float far_plane)
	{
		float tanHalfFOV = tan(45.0 * M_PI/180);
		// float S = 1.0f / (tanHalfFOV * (320.0/240.0));

		klUnitMatrix(m);
		m->data[0][0] = FX_FromFloat(1.0f / (tanHalfFOV * (320.0/240.0)));
		m->data[1][1] = FX_FromFloat(1.0f / tanHalfFOV);
		m->data[2][2] = FX_FromFloat( (near_plane + far_plane) / (near_plane - far_plane) );
		m->data[2][3] = FX_FromFloat(-1);
		m->data[3][3] = 0;
		m->data[3][2] = FX_FromFloat((2*near_plane*far_plane) / (near_plane-far_plane));
	}
}

