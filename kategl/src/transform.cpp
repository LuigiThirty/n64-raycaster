#include "kategl/transform.hpp"

namespace KateGL
{
    void klTranslation(FX_Matrix44 *m, Fixed x, Fixed y, Fixed z)
    {
        klUnitMatrix(m);
        m->data[3][0] = x;
        m->data[3][1] = y;
        m->data[3][2] = z;
    }

    void klRotationDegreesX(FX_Matrix44 *m, int degrees)
    {
        Fixed cosQ = FX_FromFloat(cosf(DegreesToRadians(degrees)));
        Fixed sinQ = FX_FromFloat(sinf(DegreesToRadians(degrees)));
        Fixed neg_sinQ = FX_Multiply(sinQ, 0xFFFF0000);

        klUnitMatrix(m);
        m->data[1][1] = cosQ;
        m->data[1][2] = sinQ;
        m->data[2][1] = neg_sinQ;
        m->data[2][2] = cosQ;
    }

    void klRotationDegreesY(FX_Matrix44 *m, int degrees)
    {
        Fixed cosQ = FX_FromFloat(cosf(DegreesToRadians(degrees)));
        Fixed sinQ = FX_FromFloat(sinf(DegreesToRadians(degrees)));
        Fixed neg_sinQ = FX_Multiply(sinQ, 0xFFFF0000);

        klUnitMatrix(m);
        m->data[0][0] = cosQ;
        m->data[0][2] = neg_sinQ;
        m->data[2][0] = sinQ;
        m->data[2][2] = cosQ;
    }

    void klRotationDegreesZ(FX_Matrix44 *m, int degrees)
    {
        Fixed cosQ = FX_FromFloat(cosf(DegreesToRadians(degrees)));
        Fixed sinQ = FX_FromFloat(sinf(DegreesToRadians(degrees)));
        Fixed neg_sinQ = FX_Multiply(sinQ, 0xFFFF0000);

        klUnitMatrix(m);
        m->data[0][0] = cosQ;
        m->data[0][1] = sinQ;
        m->data[1][0] = neg_sinQ;
        m->data[1][1] = cosQ;
    }

    void klScale(FX_Matrix44 *m, Fixed x, Fixed y, Fixed z)
    {
        klUnitMatrix(m);
        m->data[0][0] = x;
        m->data[1][1] = y;
        m->data[2][2] = z;
    }

    inline float DegreesToRadians(float degrees)
    {
    return degrees * (M_PI/180.0);
    }

    Vector2<float> RotationFromT2DXY(const Transform2D transform, const float x, const float y)
    {
    Radians rads = DegreesToRadians(transform.rotation);

    Vector2<float> rotation(0,0);
    rotation.x = x*cos(rads) - y*sin(rads);
    rotation.y = x*sin(rads) + y*cos(rads);
    return rotation;
    }
}