#include "kategl/bound.hpp"
#include "kategl/fixed.h"

bool KateGL::PointIsInsideTextureBound2D(TextureListEntry *tle, Point3D point_a, Point3D target)
{
    // Returns true if Target is inside the bounding box (TLE + PointA).
    // Does not use the Z axis.

    Point2D a { .x = FX_ToInt(point_a.x), .y = FX_ToInt(point_a.y) };
    Point2D t { .x = FX_ToInt(target.x), .y = FX_ToInt(target.y) };

    Point2D bounding_box_upleft, bounding_box_downright;
    bounding_box_upleft.x = a.x; bounding_box_upleft.y = a.y;
    bounding_box_downright.x = a.x + tle->info->size.x; bounding_box_downright.y = a.y + tle->info->size.y;

    return  t.x >= bounding_box_upleft.x && 
            t.x < bounding_box_downright.x &&
            t.y >= bounding_box_upleft.y && 
            t.y < bounding_box_downright.y;
}

bool KateGL::TexelContainsBackgroundPixel(TextureListEntry *tle, Point2D texel)
{
    // Get the texel at point TEXEL of texture TLE.
    // If the texel is the background color, return true.
    uint32_t texel_data = 0; 

    // sprintf(dbg, "pixel size is %d\n", tle->info->pixel_size);
    // _64Drive_putstring(dbg);

    switch(tle->info->pixel_size)
    {
        // TODO: Ew, multiplications and divisions. Precalculate or cache these values somehow?
        case KL_TEX_DEPTH_4BIT:
        // Two texel per byte. Row 1, Column 0 of a 32x32 texture would be byte 16.
        texel_data = ((uint8_t *)tle->info->data)[(((texel.y * tle->info->size.x) + texel.x) / 2)];
        if(texel.x & 0x01)
            texel_data = texel_data & 0x0F;
        else
            texel_data = (texel_data & 0xF0) >> 4;
        break;
        case KL_TEX_DEPTH_8BIT:
        // One texel per byte.
        texel_data = ((uint8_t *)tle->info->data)[(texel.y * tle->info->size.x) + texel.x];
        break;
        // One texel per 2 bytes.
        case KL_TEX_DEPTH_16BIT:
        texel_data = ((uint16_t *)tle->info->data)[(texel.y * tle->info->size.x) + texel.x];
        break;
        case KL_TEX_DEPTH_32BIT:
        texel_data = ((uint32_t *)tle->info->data)[(texel.y * tle->info->size.x) + texel.x];
        break;
    }

    return texel_data == 0;
}