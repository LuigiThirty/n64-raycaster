#include "kategl/blit/blit.h"
#include "64drive.h"

void klBitBlt(KLBitmap *destBitmap, int destX, int destY, KLBitmap *sourceBitmap, int sourceX, int sourceY, int sizeX, int sizeY)
{
    int startingDestOffset = (destBitmap->size.x * destY) + destX;
    int destStride = destBitmap->size.x - sizeX;
    int destCurrent = startingDestOffset;

    int startingSourceOffset = (sourceBitmap->size.x * sourceY) + sourceX;
    int sourceStride = sourceBitmap->size.x - sizeX;
    int sourceCurrent = startingSourceOffset;

    // For now, assume that the destination framebuffer is always 16bpp.

    switch(sourceBitmap->pixel_size)
    {
        case KL_TEX_DEPTH_32BIT:
        {
            // One 32-bit source word is one 16-bit destination pixel.
            // Compress an RGBA8888 pixel down to an RGBA5551 pixel.
            for(int row=0; row<sizeY; row++)
            {
                for(int col=0; col<sizeX; col++)
                {
                    uint32_t sourcePixel = ((uint32_t *)sourceBitmap->data)[sourceCurrent++];

                    // TODO: conversion table between 8-bit and 5-bit colors? 
                    // which is faster on an N64: x >> 3 (no RDRAM hit) or colortable[x] (RDRAM hit)

                    uint8_t r, g, b, a;
                    r = ((sourcePixel & 0xFF000000) >> 24) >> 3;
                    g = ((sourcePixel & 0x00FF0000) >> 16) >> 3;
                    b = ((sourcePixel & 0x0000FF00) >> 8) >> 3;
                    a = (sourcePixel & 0x000000FF) == 0 ? 0 : 1;

                    uint16_t source16bit = (r << 11) | (g << 6) | (b << 1) | a;
                    ((uint16_t *)destBitmap->data)[destCurrent++] = source16bit; 
                }

                destCurrent += destStride;
                sourceCurrent += sourceStride;
            }
        }
        case KL_TEX_DEPTH_16BIT:
        {
            // One 16-bit word is one pixel.
            for(int row=0; row<sizeY; row++)
            {
                for(int col=0; col<sizeX; col++)
                {
                    ((uint16_t *)destBitmap->data)[destCurrent++] = ((uint16_t *)sourceBitmap->data)[sourceCurrent++]; 
                }

                destCurrent += destStride;
                sourceCurrent += sourceStride;
            }
        }
    }
    

}