#include "kategl/highlevel.hpp"
#include "kategl/fixed.h"

namespace KateGL
{
    namespace DL
    {
        // DLPtr display_list_buffer = (DLPtr)malloc(4096);
        // DLPtr display_list_ptr = display_list_buffer;

        // DLPtr klGetBufferedDisplayList()
        // {
        //     char dbg[512];
        //     sprintf(dbg, "Providing display list %p\n", display_list_buffer);
        //     _64Drive_putstring(dbg);

        //     memset(display_list_buffer, 0, 0x800);
        //     display_list_ptr = display_list_buffer;
        //     return (DLPtr)UNCACHED_ADDR(display_list_ptr);
        // }

        // void klExecuteBufferedDisplayList(void *rdp_addr)
        // {
            // Is a display list executing?

            // If not, copy display_list_buffer to RDP and call run_code.

            // Otherwise spin until RDP is available, then copy and run.
            // KateGL::RCP::wait_for_rsp();
		    // KateGL::RCP::wait_for_rdp();

        //     memcpy((void *)0xA4000400, UNCACHED_ADDR(&display_list_buffer), 0x400);
        //     char dbg[512];
        //     sprintf(dbg, "Copying display list from %p to %p\n", UNCACHED_ADDR(display_list_buffer), (void *)rdp_addr);
        //     _64Drive_putstring(dbg);

        //     run_ucode();
        // }

        /* Set the Color Combiner up to draw a textured rectangle (i.e. a 2D decal) */
        void klCombineModeTexturedRectangle(DLPtr *list)
        {
            rdp_set_combine_mode(list, 
                CC_C0_RGB_MUL_ZERO_COLOR|CC_C1_RGB_MUL_ZERO_COLOR|
                CC_C0_RGB_ADD_TEXEL0_COLOR|CC_C1_RGB_ADD_TEXEL0_COLOR|
                CC_C0_ALPHA_MUL_ZERO|CC_C0_ALPHA_ADD_TEXEL0|
                CC_C1_ALPHA_MUL_ZERO|CC_C1_ALPHA_ADD_TEXEL0);
        }

        /* Kludge: bump the display list so it starts at $010 instead of $000 */
        void klPreamble(DLPtr *list)
        {
            rdp_set_default_clipping(list);
            rdp_set_default_clipping(list);
            rdp_set_default_clipping(list);
            rdp_set_default_clipping(list);
        }

        /* Perform a draw of a MObject with the specified texture. */
        void klDrawTexturedMObject(DLPtr *list, MObject *mobj, TextureListEntry *texture)
        {
            float scalex, scaley;
            if(texture->info->format == KL_TEX_FORMAT_CI)
            {
                scalex = TEXTURE_CI_SCALE_X;
                scaley = TEXTURE_CI_SCALE_Y;
            }
            else
            {
                scalex = 1.00;
                scaley = 1.00;
            }

            rdp_draw_textured_rectangle_scaled(list, TEXSLOT_0, 
                FX_ToInt(mobj->position.x), FX_ToInt(mobj->position.y), 
                FX_ToInt(mobj->position.x) + texture->info->size.x-1, FX_ToInt(mobj->position.y) + texture->info->size.y-1, 
                scalex, scaley,
                0, 0);
        }

        void klSetupForMObject(DLPtr *list)
        {
            klCombineModeTexturedRectangle(list);
            rdp_set_other_modes(list, MODE_CYCLE_TYPE_COPY|MODE_EN_TLUT|MODE_ALPHA_COMPARE_EN);
        }

        void klUseDisplayContext(DLPtr *list, display_context_t context)
        {
            rdp_attach_display(list, context);
        }

        void klSyncAndEnd(DLPtr *list)
        {
            rdp_sync(list, SYNC_FULL);
            rdp_end_display_list(list);
        }

        void klLoadSync(DLPtr *list)
        {
            rdp_sync(list, SYNC_LOAD);
        }

        void klPipeSync(DLPtr *list)
        {
            rdp_sync(list, SYNC_PIPE);
        }

        void klTileSync(DLPtr *list)
        {
            rdp_sync(list, SYNC_TILE);
        }

        void klFullSync(DLPtr *list)
        {
            rdp_sync(list, SYNC_FULL);
        }
    }
}
