#include "kategl/mobj/mobject.h"

namespace KateGL
{
    MObject::MObject()
    {
        velocity.x = 0;
        velocity.y = 0;
        velocity.z = 0;
        position.x = 0;
        position.y = 0;
        position.z = 0;
    }
}
