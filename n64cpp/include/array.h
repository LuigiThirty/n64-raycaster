#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <cstddef.h>
#include <algorithm.h>

namespace std
{
    template<class T, std::size_t N> struct array
    {
        private:
        T contents[N];

        public:
        typedef T                   value_type;
        typedef value_type*         pointer;
        typedef std::ptrdiff_t      difference_type;
        typedef value_type&         reference;
        typedef size_t              size_type;
        typedef value_type*         iterator;
        typedef const value_type*	const_iterator;

        constexpr size_type size() const { return N; }

        constexpr reference operator[](size_type pos) { return contents[pos]; }

        constexpr reference front() { return contents[0]; }
        constexpr reference back() { return contents[N-1]; }

        constexpr iterator begin() { return contents; }
        constexpr iterator end() { return contents+N; }

        constexpr T* data() { return contents; }

        void fill(const T& value)
        {
            std::for_each(begin(), end(), [&value](auto &arrval)
            {
                arrval = value;
            });
        }
    };
}

#endif