#ifndef CSTDDEF_HPP
#define CSTDDEF_HPP

namespace std 
{
    typedef unsigned int size_t;
    typedef unsigned int ptrdiff_t;
}

#endif