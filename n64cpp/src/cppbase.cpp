#include <stddef.h>
#include <libdragon.h>

#include "cppbase.h"
 
void *operator new(size_t size)
{
    return malloc(size);
}
 
void *operator new[](size_t size)
{
    return malloc(size);
}
 
void operator delete(void *p)
{
    free(p);
}
 
void operator delete[](void *p)
{
    free(p);
}

void operator delete(void *p, unsigned int size)
{
    free(p);
}

void __cxa_pure_virtual()
{
    while(1);
}